<?php

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::query()->truncate();

        $currency = new Currency();
        $currency->name = 'United States Dollar';
        $currency->symbol = '$';
        $currency->rate = 1;
        $currency->country_id = 1;
        $currency->save();

        $currency = new Currency();
        $currency->name = 'Derham';
        $currency->symbol = 'AED';
        $currency->rate = 0.74;
        $currency->country_id = 2;
        $currency->save();

        $currency = new Currency();
        $currency->name = 'Lebanese pound';
        $currency->symbol = 'L.L.';
        $currency->rate = 1515;
        $currency->country_id = 3;
        $currency->save();

        $currency = new Currency();
        $currency->name = 'Saudi Riyal';
        $currency->symbol = 'SAR';
        $currency->rate = 0.3;
        $currency->country_id = 2;
        $currency->save();
    }
}
