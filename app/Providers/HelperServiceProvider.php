<?php

namespace App\Providers;

use App\Models\Company;
use App\Utils\PermissionHelper;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    use PermissionHelper;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['cms.layouts.sidebar'], function ($view) {
            $view->with('menuItems', $this->getMenu());
        });

        view()->composer(['admin.layouts.sidebar'], function ($view) {
            $view->with('menuItems', $this->getAdminMenu());
        });
        try {
            $company = null;
        } catch (\Exception $e) {
            $company = null;
        }

        view()->composer(['cms.layouts.form.javascript', 'cms.layouts.datatable.javascript', 'cms.layouts.form.elements.file'], function ($view) use ($company) {
            $view->with('company', $company);
        });
    }

    public function getMenu()
    {
        $menu = [
            'dashboard' => [
                'display' => 'Dashboard',
                'icon' => 'fa fa-dashboard',
                'allowed_roles'=>['dev','admin'],
                'href' => route('dashboard')
            ],

            'frontend' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'Front Management',
                'icon' => 'fa fa-tasks',
                'submenu'=>[
                    'home' => [
                        'display' => 'Home',
                        'href' => route('home.index'),
                    ],
                    'solution' => [
                        'display' => 'Solutions',
                        'href' => route('solution.index'),
                    ],
                    'feature' => [
                        'display' => 'Features',
                        'href' => route('feature.index')
                    ],
                    'about' => [
                        'display' => 'About',
                        'href' => route('about.index')
                    ],
                    'about-values' => [
                        'display' => 'About-Values',
                        'href' => route('value.index')
                    ],
                    'about-team' => [
                        'display' => 'About-Team',
                        'href' => route('team.index')
                    ],
                    'registration' => [
                        'display' => 'Registration',
                        'href' => route('registration.index')
                    ],
                    'registration-benefits' => [
                        'display' => 'Registration-Benefits',
                        'href' => route('benefit.index')
                    ]
                ]
            ],



            'contract' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'Contract Management',
                'icon' => 'fa fa-tasks',
                'submenu'=>[
                    'contract' => [
                        'display' => 'Contract Templates',
                        'href' => route('contract.index'),
                    ],
                    'venue-contracts' => [
                        'display' => 'Venue Contracts',
                        'href' => route('venue-contract.index'),
                    ],
                    'trainer-contract' => [
                        'display' => 'Trainer Contracts',
                        'href' => 'javascript:void(0)',
                    ],
                ],
            ],
            'user' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'User Management',
                'icon' => 'fa fa-tasks',
                'href'=>route('user.index')
            ],

            'venue' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'Venue Management',
                'icon' => 'fa fa-tasks',
                'href'=>route('venue.index')
            ],

            'trainer' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'Trainer Management',
                'icon' => 'fa fa-tasks',
                'href'=> 'javascript:void(0)'
            ],
            'role' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'Roles Management',
                'icon' => 'fa fa-tasks',
                'href'=>route('role.index')
            ],
            'umarket' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'uMarket Management',
                'icon' => 'fa fa-tasks',
                'href'=>'javascript:void(0)'
            ],
            'spread' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'uSpread Management',
                'icon' => 'fa fa-tasks',
                'href'=>'javascript:void(0)'
            ],
            'payment' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'Payment Management',
                'icon' => 'fa fa-tasks',
                'href'=>'javascript:void(0)'
            ],
            'content' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'Content Management',
                'icon' => 'fa fa-tasks',
                'submenu'=>[
                    'category' => [
                        'display' => 'Categories',
                        'href' => route('category.index'),
                    ],
                    'sport' => [
                        'display' => 'Sports',
                        'href' => route('sport.index'),
                    ],
                    'zone' => [
                        'display' => 'Zones',
                        'href' => route('zone.index')
                    ],
                    'membership-type' => [
                        'display' => 'Membership Types',
                        'href' => route('membership_type.index')
                    ],
                    'country' => [
                        'display' => 'Countries',
                        'href' => route('country.index')
                    ],
                    'location' => [
                        'display' => 'Locations',
                        'href' => route('location.index')
                    ],
                    'selection' => [
                        'display' => 'Selection',
                        'href' => route('selection.index')
                    ],
                    'currency' => [
                        'display' => 'Currencies',
                        'href' => route('currency.index')
                    ]
                ]
            ],
            'notification' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['dev','admin'],
                'display' => 'Notifications Management',
                'icon' => 'fa fa-tasks',
                'href'=>'javascript:void(0)'
            ],


        ];
       // ksort($menu['management']['submenu']);
        return $this->drawMenu($menu);
    }


    public function getAdminMenu()
    {

        $menu = [
            'dashboard' => [
                'display' => 'My Wall',
                'icon' => 'fa fa-dashboard',
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'href' => route('admin.dashboard')
            ],
            'venue' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'My Venues',
                'icon' => 'fa fa-tasks',
                'href'=>route('admin.venue.index')
            ],
            'booking' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'Trainer Management',
                'icon' => 'fa fa-tasks',
                'href'=> route('admin.trainer.index')
            ],
            'market' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'uMarket',
                'icon' => 'fa fa-tasks',
                'href'=> 'javascript:void(0)'
            ],
            'spread' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'uSpread',
                'icon' => 'fa fa-tasks',
                'href'=> 'javascript:void(0)'
            ],
            'payment' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'Payments',
                'icon' => 'fa fa-tasks',
                'href'=> 'javascript:void(0)'
            ],
            'user' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'Users',
                'icon' => 'fa fa-tasks',
                'href'=>'javascript:void(0)'
            ],
            'role' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'Roles',
                'icon' => 'fa fa-tasks',
                'href'=>'javascript:void(0)'
            ],
            'about' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'About us',
                'icon' => 'fa fa-tasks',
                'href'=>'javascript:void(0)'
            ],
            'contact' => [
                'allow_permission_check' => true,//flag to check permission or not, note if not found, it is true
                'allowed_roles'=>['venue-owner','venue-moderator','dev'],
                'display' => 'Contact us',
                'icon' => 'fa fa-tasks',
                'href'=>'javascript:void(0)'
            ],

        ];
        // ksort($menu['management']['submenu']);
        return $this->drawMenu($menu);
    }

    function drawMenu($menu, $flag_first_time = true)
    {

        static $menu_view = "";
        $menu_view .= !$flag_first_time ? '<ul class="nav child_menu">' : '<ul class="nav side-menu">';
        foreach ($menu as $key => $value) {
            $icon = !isset($value['icon']) ? "" : "<i class='{$value['icon']}'></i>";
            if ( !$this->allowedRole(request()->permission_name ,
                Arr::has($value,'allowed_roles')? $value['allowed_roles']:[]) )
                continue;
            if ( Arr::has($value, 'submenu')){
                if (!isset($value['submenu'])) {
                    if ((!isset($value['allow_permission_check'])
                            && $this->hasPermissions(request()->permissions, $key))
                        || (isset($value['allow_permission_check'])
                            && $value['allow_permission_check']
                            && $this->hasPermissions(request()->permissions, $key)))
                        $menu_view .=
                            "<li><a href='{$value['href']}'>$icon {$value['display']}</a></li>";
                } else {
                    if ((!isset($value['allow_permission_check'])
                            && $this->hasPermissions(request()->permissions, $key))
                        || (isset($value['allow_permission_check'])
                            && $value['allow_permission_check']
                            && $this->hasPermissions(request()->permissions, $key)))
                    $menu_view .=
                        "<li><a>$icon {$value['display']}<span class='fa fa-chevron-down'></span></a>";
                    $this->drawMenu($value['submenu'], false);
                    $menu_view .= '</li>';
                }
            }else{
                if ((!isset($value['allow_permission_check'])
                        && $this->hasPermissions(request()->permissions, $key))
                    || (isset($value['allow_permission_check'])
                        && $value['allow_permission_check']
                        && $this->hasPermissions(request()->permissions, $key)))
                $menu_view .=
                    "<li><a href='{$value['href']}'>$icon {$value['display']}</a></li>";
            }

        }
        $menu_view .= '</ul>';
        return $menu_view;
    }
}
