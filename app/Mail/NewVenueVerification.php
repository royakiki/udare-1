<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewVenueVerification extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $venue;

    public function __construct($user,$venue)
    {
        $this->user = $user;
        $this->venue = $venue;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.verify-venue', ['user'=>$this->user,'venue'=>$this->venue]);
    }
}
