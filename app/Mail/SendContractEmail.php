<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendContractEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $venueContract;

    public function __construct($venueContract)
    {
        $this->venueContract = $venueContract;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.send-contract', ['venueContract'=>$this->venueContract]);
    }
}
