<?php

namespace App;

use App\Models\Currency;
use App\Models\Role;
use App\Models\Status;
use App\Models\Venue;
use Carbon\Carbon;
use App\Utils\Passport\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

//use Illuminate\Contracts\Auth\Authenticatable as Auth;

class User extends Authenticatable implements CanResetPasswordContract
{
    use Notifiable, CanResetPassword,SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email',
        'password', 'phone', 'gender',
        'dob', 'role_id', 'provider_id','status_id',
        'provider','country_code','email_verified',
        'email_verified_at','email_verification_token'
    ];

//    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'provider_id', 'device_token', 'reg_id',
         'remember_token', 'device_platform','status_id',
        'email_verified_at', 'deleted_at', 'created_at', 'updated_at','email_verified','email_verification_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with=['role'];

    public $formFields;

    public $fields = [
        [
            'key' => 'name',
            'title' => 'Name',
            'type' => 'field',
            'db_name' => 'users.name'
        ],
    ];

    public function __construct(array $attributes = [])
    {
        $this->formFields = [

        ];
        parent::__construct($attributes);
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function venues(){
        return $this->hasMany( Venue::class,'owner_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['dob'])->age;
    }


    public function relatedVenues(){
        if ( $this->role->slug == 'dev'){
            return Venue::query()->get()->pluck('_id');
        }else{
            return $this->venue_ids == null ? [] : $this->venue_ids;
        }
    }
    public function moderatorVenues(){
        return $this->belongsToMany(Venue::class );
    }


    public function  status(){
        return $this->belongsTo(Status::class);
    }

    public function allowToAccessAdmin(){
        $id = $this->id;
        $venues = Venue::query()->whereHas('status', function($q){
            $q->where('slug','!=','pending');
        })->where(function ($q) use ($id){
            return $q->where('moderator_ids','all',[$id])->orWhere('trainer_ids','all',[$id])->orWhere('owner_id',$id);
        })->count();

        if ($this->role->slug == 'dev')
            return true;

        return $this->status->slug == 'active' && $venues > 0 ;
    }


}
