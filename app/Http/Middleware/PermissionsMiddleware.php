<?php

namespace App\Http\Middleware;

use App\Models\Site;
use Closure;
use Exception;
use Illuminate\Http\Request;

class PermissionsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jsonString = file_get_contents(base_path("config/permissions.php"));
        $permissions = eval('?>' . $jsonString);

        try {
            $request->permissions = $permissions[auth()->user()->role->slug];
            $request->permission_name = auth()->user()->role->slug;
        } catch (Exception $e) {
            $request->permissions = null;
        }
        return $next($request);
    }
}
