<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VenueChecker
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check() && (in_array(Auth::user()->role->slug,['venue-owner','venue-moderator','dev']))){
            if ( !Auth::user()->allowToAccessAdmin() ){
                Auth::logout();
                toastr('you account is not valid or your venue still not approved','error');
                return redirect()->route('login');
            }else{
                return $next($request);
            }
        }

        else {
            if (!$request->isJson() ) {
                Auth::logout();
                return redirect()->route('login');
            } else {
                return response()->json(['message' => 'not authorized'], 401);
            }

        }
    }
}
