<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Selection;
use App\Models\Zone;
use Illuminate\Http\Request;

class SelectionController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Selection());
    }
}
