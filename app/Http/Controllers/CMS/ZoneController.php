<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Zone;
use Illuminate\Http\Request;

class ZoneController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Zone());
    }
}
