<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Country;
use App\Models\Field;
use App\Models\Location;
use App\Models\Selection;
use App\Models\Venue;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class VenueController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Venue());
    }

    public function create()
    {

        $categories = Category::all();

        return view('cms.venue.create',[
            'categories' => $categories,
            'title' => $this->compacts['title'],
            'route' => $this->module->route,
        ]);
    }
    public function store(Request $request)
    {

        $this->checkPermission($request->permissions, $this->module->route, 'edit');
        //$this->validationRules['name'] .= ',' . $id;
//        $this->validate($request, $this->validationRules);
//       $this->customValidate($request, ['ahmad'=>'required', 'rabee'=>'required|email']);
        $allParams = $request->except('_method', '_token');
        $params = [];
        $m = new Venue();
        $m->email = $request->input('email');
        $m->name = $request->input('name');
        $m->commercial_name = $request->input('commercial_name');
        $m->owner_id = $request->input('owner_id');
        $m->country_id = $request->input('country_id');
        $m->location_id = $request->input('location_id');
        $m->status_id = $request->input('status_id');

        $m->sports()->sync(is_array($request->input('sport_ids'))?$request->input('sport_ids'):[$request->input('sport_ids')]);
        $m->selections()->sync(is_array($request->input('selection_ids'))?$request->input('selection_ids'):[$request->input('selection_ids')]);
        $m->save();

        return redirect()->route('venue.index');
    }

    public function edit($id)
    {
        $venue = Venue::findOrFail($id);
        $categories = Category::all();
        return view('cms.venue.edit', [
            'venue' => $venue,
            'categories'=>$categories,
            ],
        $this->compacts
        );
    }


    public function update(Request $request, $id)
    {
        $this->checkPermission($request->permissions, $this->module->route, 'edit');
        //$this->validationRules['name'] .= ',' . $id;
//        $this->validate($request, $this->validationRules);
        $allParams = $request->except('_method', '_token');
        $params = [];
        $m = Venue::query()->findOrFail($id);
        $m->email = $request->input('email');
        $m->name = $request->input('name');
        $m->commercial_name = $request->input('commercial_name');
        $m->owner_id = $request->input('owner_id');
        $m->country_id = $request->input('country_id');
        $m->location_id = $request->input('location_id');
        $m->status_id = $request->input('status_id');

        $m->sports()->sync(is_array($request->input('sport_ids'))?$request->input('sport_ids'):[$request->input('sport_ids')]);
        $m->selections()->sync(is_array($request->input('selection_ids'))?$request->input('selection_ids'):[$request->input('selection_ids')]);
        $m->save();

        return redirect()->route('venue.index');
    }

}
