<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\AppUser;
use App\Models\Category;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TrainerController extends Controller
{
    public function __construct()
    {
        $this->compacts = [
            'title' => 'TRAINERS',
            'fields' => (new AppUser)->fields,
            'route' => route('trainer.index'),
            'new' => route('trainer.create'),
        ];
        $this->validationRules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
//            'phone' => 'required',
            'gender' => 'required|in:male,female,not specified',
            'password' => 'required|confirmed|min:6',
            'role_id' => 'required|exists:roles,id',
            'company_id' => 'required|exists:companies,id',
        ];
    }
    public function index(Request $request, $type = null)
    {
        $permissions = $this->getPermissions($request->permissions, 'trainer');
//        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions))
            unset($this->compacts['new']);
        $this->compacts['route'] = route('trainer.index');
        if (!is_null($type)) {
            unset($this->compacts['new']);
            $permissions = ['show'];
        }
        $user = new AppUser();
        if ($request->ajax()) {
//           if($request->status != null)
//               $status=$request->status;
//           else
//               $status="initial";
            $role = Role::query()->where('name', 'Trainer')->first()->_id;
            $query = AppUser::query()->where('role_id', '=', $role);
            return $user->renderDataTable($query, $permissions);
        }

        return view('cms.layouts.resources.index', $this->compacts);
    }

    public function edit($id)
    {
        dd();
        $trainer = User::findOrFail($id);
        $categories = Category::all();
        return view('cms.trainer.edit', [
            'trainer' => $trainer,
            'categories'=>$categories,
        ]
        );
    }


}
