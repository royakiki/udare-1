<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Category());
    }
}
