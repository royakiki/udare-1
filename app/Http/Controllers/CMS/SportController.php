<?php

namespace App\Http\Controllers\CMS;

use App\Models\Sport;
use Illuminate\Http\Request;

class SportController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Sport());
    }
}
