<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\AppUser;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->compacts = [
            'title' => 'USERS',
            'fields' => (new AppUser)->fields,
            'route' => route('user.index'),
            'new' => route('user.create'),
        ];
        $this->validationRules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
//            'phone' => 'required',
            'gender' => 'required|in:male,female,not specified',
            'password' => 'required|confirmed|min:6',
            'role_id' => 'required|exists:roles,id',
            'company_id' => 'required|exists:companies,id',
        ];
    }

    public function index(Request $request, $type = null)
    {
        $permissions = $this->getPermissions($request->permissions, 'user');
//        if (in_array('new', array_keys($this->compacts)) && !in_array('add', $permissions))
            unset($this->compacts['new']);
        $this->compacts['route'] = route('user.index');
        if (!is_null($type)) {
            unset($this->compacts['new']);
            $permissions = ['show'];
        }
        $user = new AppUser();
        if ($request->ajax()) {
            $role = Role::query()->where('name', 'User')->first()->_id;
            $query = AppUser::query()->where('role_id', '=', $role);
            return $user->renderDataTable($query, $permissions);
        }

        return view('cms.layouts.resources.index', $this->compacts);
    }


    public function create()
    {
        $this->checkPermission(request()->permissions, 'user', 'add');
        $form = (new AppUser)->renderForm(route('user.store'));
        return view('cms.user.create', compact('form'), ['title' => $this->compacts['title']]);
    }

    public function store(Request $request)
    {
        $this->checkPermission($request->permissions, 'user', 'add');
        $this->validate($request, $this->validationRules);
        $params = $request->except('_token', '_method');
        $user_id = AppUser::query()->create($params)->id;
        AppUser::query()->find($user_id)->sites()->sync($request->site_id);
        return redirect()->route('user.index');
    }

    public function show($id)
    {
        $compact = ['title' => $this->compacts['title']];
        if (AppUser::query()->find($id)->role->isClient())
            $compact = ['title' => 'CLIENTS', 'disable_list_link' => false];
        $this->checkPermission(request()->permissions, 'user', 'show');
        $form = (new AppUser)->renderForm(route('user.update', $id), $id, 1,
            false, []);
        return view('cms.layouts.resources.show', compact('form'), $compact);
    }

    public function edit($id)
    {
        $this->checkPermission(request()->permissions, 'user', 'edit');
        $form = (new AppUser)->renderForm(route('user.update', $id), $id);
        return view('cms.user.edit', compact('form'), ['title' => $this->compacts['title'],'route'=>'user']);
    }

    public function update(Request $request, $id)
    {
        $this->checkPermission($request->permissions, 'user', 'edit');
        $this->validationRules['username'] .= ',' . $id;
        $this->validationRules['email'] .= ',' . $id;
        if ($request->password == "") {
            unset($this->validationRules['password']);
            $request->request->remove('password');
        }
        $this->validate($request, $this->validationRules);
        $params = $request->except('_method', '_token');
        AppUser::query()->findOrFail($id)->update($params);

        return redirect()->route('user.index');
    }

    public function destroy($id)
    {
        AppUser::query()->find($id)->sites()->detach();
        AppUser::destroy($id);
    }

    public function profile()
    {
        return view('cms.user.profile');
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users,username,' . auth()->user()->id,
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . auth()->user()->id,
        ]);
        $params = $request->except('_token', '_method');

        auth()->user()->update($params);
        return view('cms.user.profile');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);
        if (Hash::check($request->old_password, auth()->user()->password)) {
            auth()->user()->update(['password' => $request->password]);
            return redirect()->back()->with('updated', true);
        }
        return redirect()->back()->with('updated', false);

    }

}
