<?php

namespace App\Http\Controllers\CMS\Front;


use App\Http\Controllers\CMS\MultiController;
use App\Models\Benefit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BenefitsController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Benefit());
    }
    public function store(Request $request)
    {
        $solution = new Benefit();
        $solution->name = $request->name;
        $solution->body = $request->body;
        $file = $request->file('image');
        $name = 'benefit_' . time() . '.' . $file->getClientOriginalExtension();
        if (!Storage::disk('public')->exists('benefit')) {
            Storage::disk('public')->makeDirectory('benefit');
        }
        if (Storage::disk('public')->putFileAs('benefit', $file, $name)) {
            $solution->image = 'benefit/' . $name;
        } else {
            return error();
        }
        $solution->save();
        return redirect()->route('benefit.index');
    }
}

