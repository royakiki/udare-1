<?php

namespace App\Http\Controllers\CMS\Front;


use App\Http\Controllers\CMS\MultiController;
use App\Models\Solution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SolutionController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Solution());
    }


//    public function create(){
//        return view('cms.front.solution.create');
//    }
    public function store(Request $request)
    {
        $solution = new Solution();
            $solution->name = $request->name;
            $solution->body = $request->body;
            $file = $request->file('image');
            $name = 'solution_' . time() . '.' . $file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('solution')) {
                Storage::disk('public')->makeDirectory('solution');
            }
            if (Storage::disk('public')->putFileAs('solution', $file, $name)) {
                $solution->image = 'storage/solution/' . $name;
            } else {
                return error();
            }
        $solution->save();
        return redirect()->route('solution.index');
    }
//    public function edit($id)
//    {
//        $solution = Solution::findOrFail($id);
//        return view('cms.front.solution.edit', compact('solution'));
//    }
//    public function update(Request $request,$id){
//        $solution = Solution::findOrFail($id);
//        return redirect()->route('solution.index');
//    }
}

