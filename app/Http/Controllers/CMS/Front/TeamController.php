<?php

namespace App\Http\Controllers\CMS\Front;


use App\Http\Controllers\CMS\MultiController;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TeamController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Team());
    }
    public function store(Request $request)
    {
        $solution = new Team();
        $solution->name = $request->name;
        $solution->body = $request->body;
        $file = $request->file('image');
        $name = 'team_' . time() . '.' . $file->getClientOriginalExtension();
        if (!Storage::disk('public')->exists('team')) {
            Storage::disk('public')->makeDirectory('team');
        }
        if (Storage::disk('public')->putFileAs('solution', $file, $name)) {
            $solution->image = 'team/' . $name;
        } else {
            return error();
        }
        $solution->save();
        return redirect()->route('team.index');
    }
}

