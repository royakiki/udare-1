<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\Venue;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ContractController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new Contract());
    }


    public function create()
    {
        $this->checkPermission(request()->permissions, $this->module->route, 'add');

        return view('cms.contract.create', ['title' => $this->compacts['title'],
            'route' => $this->module->route]);
    }

    public function store(Request $request)
    {
        $this->checkPermission($request->permissions, $this->module->route, 'add');
        $params = $this->validate($request, [
            'name'=>'required',
            'description'=>'required',
            'country_id'=>'required',
            'status_id'=>'required',
        ]);

        $m = Contract::query()->create($params);

//        foreach ($request->input('documents', []) as $file) {
//            $m->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('document');
//        }

        $m->save();
        return redirect()->route($this->module->route.'.index');
    }


    public function edit($id)
    {
        $this->checkPermission(request()->permissions, $this->module->route, 'edit');
        $contract = Contract::query()->find($id);
        return view('cms.contract.edit', compact('contract'),
            ['title' => $this->compacts['title'],'route' => $this->module->route]);
    }

    public function update(Request $request, $id)
    {
        $this->checkPermission($request->permissions, $this->module->route, 'edit');

        $params = $this->validate($request, [
            'name'=>'required',
            'description'=>'required',
            'country_id'=>'required',
            'status_id'=>'required',
        ]);

        Contract::query()->find($id)->update($params);

        return redirect()->route($this->module->route.'.index');
    }


}
