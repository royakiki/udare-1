<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\MembershipType;
use App\Models\Zone;
use Illuminate\Http\Request;

class MembershipTypeController extends MultiController
{
    //
    public function __construct()
    {
        parent::__construct(new MembershipType());
    }
}
