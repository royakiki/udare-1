<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Shift;
use App\Models\Site;
use App\Models\Transaction;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use ErrorException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Raulr\GooglePlayScraper\Scraper;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    public function index(Request $request)
    {


        return view('admin.dashboard.index');
    }

    public function getSiteTransactions(Request $request)
    {
        $sites = Site::query()->whereIn('id', $request->allowed_sites)->get();
        foreach ($sites as $site) {
            $site->transaction_number = $site->getLiveTransactions();
        }
        return json_encode($sites);
    }

    public function reportsIndex($shift_id = null)
    {
//        $sites = auth()->user()->sites;
        $shift = $shift_id != null ? Shift::query()->find($shift_id) : [];
        $sites = Site::all();
        $services = Service::query()->whereHas('sites')->whereDoesntHave('parent')->get();
        $employees = User::query()->employees()->get();
        $clients = User::query()->clients()->get();
        return view('admin.reports.reports', compact('sites', 'services', 'employees', 'clients', 'shift'));
    }

    public function generateReport(Request $request)
    {
        list($from, $to) = explode(' - ', $request->from_to);
        $query = Transaction::query()
            ->where('created_at', '>=', Carbon::parse($from)->toDateTimeString())
            ->where('created_at', '<=', Carbon::parse($to)->toDateTimeString())
            ->whereDoesntHave('parent')
            ->orderBy('created_at', 'DESC');

        //filters
        if ($request->has('sites') && !is_null($request->sites))
            $query->where('site_id', $request->sites);
        if ($request->has('services') && !is_null($request->services))
            $query->where('service_id', $request->services);
        if ($request->has('shift') && !is_null($request->shift)) {
            $shift = Shift::query()->find($request->shift);
            if (is_null($shift))
                return response()->json(['error' => 'invalid'], 422);
            $query->shiftTransactions($shift->start_date, $shift->end_date);
        }
        if ($request->has('employees') && !is_null($request->employees))
            $query->whereHas('statuses', function ($q) use ($request) {
                $q->whereUserId($request->employees)->orWhere('handled_by', $request->employees);
            });
        if ($request->has('clients') && !is_null($request->clients))
            $query->whereHas('user', function ($q) use ($request) {
                $q->clients()->whereClientId($request->clients);
            });
        //end filters

        if ($request->statistics_type == 'graphs') {
            $date_format = '';
            $chart_labels = [];
            $chart_values = [];
            if ($request->has('x_axis')) {
                if ($request->x_axis == 'day') {
                    $date_format = 'Y-m-d';
                    $date_period = CarbonPeriod::create(Carbon::parse($from), Carbon::parse($to));
                    foreach ($date_period as $date)
                        $chart_labels[] = $date->format($date_format);
                } elseif ($request->x_axis == 'month') {
                    $date_format = 'F-Y';
                    $date_period = CarbonPeriod::create(Carbon::parse($from), '1 month', Carbon::parse($to));
                    foreach ($date_period as $date)
                        $chart_labels[] = $date->format($date_format);
                } elseif ($request->x_axis == 'quarter') {
                    $date_format = 'quarter';
                    $date_period = CarbonPeriod::create(Carbon::parse($from), Carbon::parse($to));
                    foreach ($date_period as $date)
                        $chart_labels[] = $date->quarter . '-' . $date->year;
                    $chart_labels = array_values(array_unique($chart_labels));//since array unique return keyed array, so use array_values
//                    sort($chart_labels);//to sort the quarters {1 2 3 4}
                    $custom_labels = $chart_labels;
                    array_walk($custom_labels, function (&$item) {
                        $item = 'Q' . $item;
                    });
                }
            }
            if (isset($request->y_axis)) {
                $chart_values_query_result = $query->get()->groupBy(function ($date) use ($chart_labels, $date_format) {
                    if ($date_format == 'quarter')
                        return Carbon::parse($date->created_at)->{$date_format} . '-' . Carbon::parse($date->created_at)->year;
                    return Carbon::parse($date->created_at)->format($date_format);
                })->toArray();
                switch ($request->y_axis) {
                    case 'count':
                        foreach ($chart_labels as $k)
                            $chart_values[] = Arr::has($chart_values_query_result, $k) ? sizeof($chart_values_query_result[$k]) : 0;
                        return response()->json(['labels' => $custom_labels ?? $chart_labels, 'values' => $chart_values, 'y_label' => 'Tr.']);
                        break;
                    case 'price':
                        foreach ($chart_labels as $k) {
                            $chart_values[$k] = 0;
                            if (Arr::has($chart_values_query_result, $k))
                                foreach ($chart_values_query_result[$k] as $item)
                                    $chart_values[$k] += $item['price'];
                            else
                                $chart_values[$k] = 0;
                        }
                        return response()->json(['labels' => $custom_labels ?? $chart_labels, 'values' => array_values($chart_values), 'y_label' => 'Price']);
                        break;
                }
            }
        } elseif ($request->statistics_type == 'table') {
            $datatable = Datatables::of($query->select('transactions.*'))
                ->addIndexColumn()
                ->addColumn('duration', function ($row) {
                    if ($row->statuses()->where('status_id', 6)->first() != null)
                        return Carbon::parse($row->created_at)->diffInMinutes($row->statuses()->where('status_id', 6)->first()->pivot->created_at) . ' minutes';
                    return 'Still inside';
                });
            return $datatable->make(true);
        }
    }

    public function reportsAjax(Request $request)
    {
        if ($request->ajax()) {
            $query = Transaction::query()->whereDoesntHave('parent');
            if ($request->has('site_id'))
                $query->where('site_id', $request->site_id);
            if ($request->has('service_id')) {
                if ($request->service_id != 1 && $request->service_id != 2)
                    $query->whereIn('service_id',
                        Service::query()->find($request->service_id)->children->pluck('id')->toArray());
                else
                    $query->where('service_id', $request->service_id);
            }
            if ($request->has('from'))
                $query->whereBetween('created_at', [$request->from, $request->to]);
            return json_encode($query->get());
        }
        return;
    }



    public function ajaxSelectByModel(Request $request, $model)
    {
        if ( $model == 'User')
            $model = 'App\User';
        else $model = 'App\Models\\'.$model;

        $model = new $model();
        $data = $model::query();

        foreach ( $request->all()  as $key => $value ){
            if ( $key != '_type' && $key !='term')
            if ($key == 'q') {
                $data->where('name', 'like', '%' . $value . '%');
            } else{
                $data->where($key, $value);
            }
        }
        return json_encode($data->orderBy('name', 'asc')->get());
    }


    public function storeMedia(Request $request)
    {
        $path = public_path('storage/tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

}
