<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use App\Models\Field;
use App\Models\Sport;
use App\Models\TimeSlot;
use App\Models\Venue;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Ramsey\Uuid\Type\Time;

class TimeSlotController extends Controller
{

    protected $module;
    protected $validationRules;

    public function __construct()
    {
        $model = new TimeSlot();

        $this->module = $model;
    }



    public function index( Request $request , $field_id){
        $time = new TimeSlot();
        $fieldObject = Field::query()->findOrFail($field_id);
        $compacts = [
                    'title' => $fieldObject->name."'s Time Slots",
                    'fields' => $time->fields,
                    'route'=> route('admin.slot.index', $field_id),
                    'new' => route('admin.slot.create', $field_id),
        ];
        $permissions = $this->getPermissions($request->permissions, $time->route);
        $baseRoute =route('admin.slot.index', $field_id);
        if (in_array('new', array_keys($compacts) ) && !in_array('add', $permissions))
            unset( $compacts['new']);
        if ($request->ajax()) {
            if ( $request->input('day') != null )
                return $time->renderDataTable(TimeSlot::query()
                    ->where('field_id',$field_id)->where('day',$request->input('day')), $permissions,$baseRoute);
            return $time->renderDataTable(TimeSlot::query()->where('field_id',$field_id), $permissions,$baseRoute);
        }
        return view('admin.layouts.resources.index', $compacts);
    }
    public function create($field)
    {
        $time = new TimeSlot();
        $fieldObject = Field::query()->findOrFail($field);
        $this->checkPermission(request()->permissions, $this->module->route, 'add');
        $compacts = [
            'title' => $fieldObject->name."'s Time Slots",
            'fields' => $time->fields,
            'route'=> route('admin.slot.index', $field),
            'new' => route('admin.slot.create', $field),
        ];
        $form = $this->module->renderForm(route('admin.slot.store',$field));
        return view('admin.venue.property.slot.create', compact('form'), ['title' => $compacts['title'],
            'route' => route('admin.slot.index',$field)]);
    }
    private function addMinutesToString($time , $minutes){
        $timeArray = explode(":",$time);
        if ( $minutes + $timeArray[1] > 59 ){
            $h = $timeArray[0] + ((int) ($minutes + $timeArray[1])/60);
            $m = ($minutes + $timeArray[1]) % 60;
        }else{
            $h = $timeArray[0];
            $m = $timeArray[1] + $minutes;
        }
        return $h.':'.$m;
    }

    public function store(Request $request, $field)
    {
        $field = Field::findOrFail($field);

        if ( $field->sport->attendance_type =='single'){
            $this->validate($request,['max'=>'required']);
        }
        if ( $field->sport->attendance_type =='multi'){
            $this->validate($request,['booking_limit'=>'required']);
        }
        $params = $request->only('day','start_time', 'end_time','price','status_id',
            'booking_limit','break_minutes','description','expertise_level','selection','max','min');
        if ( strcmp($params['start_time'],$params['end_time']) >= 0
            || strcmp($this->addMinutesToString($params['start_time'],
                $params['break_minutes']==null ? 0 : $params['break_minutes']),$params['end_time']) >= 0  ){
            toastr("wrong selection for dates",'error');
            return redirect()->back();
        }

        $availability=  TimeSlot::query()
            ->where('field_id',$field->_id)
            ->where('day',$params['day'])
            ->where(function ($query) use ($params) {
                $query
                    ->where( function($q) use ($params){
                        $q->where('start_time','>=',$params['start_time'] );
                        $q->where('start_time','<=',$params['end_time']);
                    });
                $query
                    ->orWhere( function($q) use ($params){
                        $q->where('end_time','>=',$params['start_time'] );
                        $q->where('end_time','<=',$params['end_time']);
                    });
            })->count();

        if ( $availability > 0){
            toastr("slot not available",'error');
            return redirect()->back();
        }
        $params['field_id'] = $field->_id;

        TimeSlot::create($params);

        return redirect()->route('admin.field.edit',['venue'=>$field->venue_id ,
            'field'=>$field->_id]);
    }

    public function edit($field ,$id)
    {
        $time = new TimeSlot();
        $fieldObject = Field::query()->findOrFail($field);
        $this->checkPermission(request()->permissions, $this->module->route, 'add');
        $compacts = [
            'title' => $fieldObject->name."'s Time Slots",
            'fields' => $time->fields,
            'route'=> route('admin.slot.index', $field),
            'new' => route('admin.slot.create', $field),
        ];
        $form = $this->module->renderForm(route('admin.slot.update',['field'=>$field,'slot'=>$id]),$id);
        return view('admin.venue.property.slot.edit', compact('form'), ['title' => $compacts['title'],
            'route' => route('admin.slot.index',$field)]);

    }
    public function update(Request $request, $field , $id)
    {
        $field = Field::findOrFail($field);

        if ( $field->sport->attendance_type =='single'){
            $this->validate($request,['max'=>'required']);
        }
        if ( $field->sport->attendance_type =='multi'){
            $this->validate($request,['booking_limit'=>'required']);
        }
        $params = $request->only('day','start_time', 'end_time','price','status_id',
            'booking_limit','break_minutes','description','expertise_level','selection','max','min');
        if ( strcmp($params['start_time'],$params['end_time']) >= 0
            || strcmp($this->addMinutesToString($params['start_time'],
                $params['break_minutes']==null ? 0 : $params['break_minutes']),$params['end_time']) >= 0  ){
            toastr("wrong selection for dates",'error');
            return redirect()->back();
        }

        $availability=  TimeSlot::query()
            ->where('field_id',$field->_id)
            ->where('day',$params['day'])
            ->where('_id','!=',$id)
            ->where(function ($query) use ($params) {
                $query
                    ->where( function($q) use ($params){
                        $q->where('start_time','>=',$params['start_time'] );
                        $q->where('start_time','<=',$params['end_time']);
                    });
                $query
                    ->orWhere( function($q) use ($params){
                        $q->where('end_time','>=',$params['start_time'] );
                        $q->where('end_time','<=',$params['end_time']);
                    });
            })->count();

        if ( $availability > 0){
            toastr("slot not available",'error');
            return redirect()->back();
        }
        $params['field_id'] = $field->_id;

        TimeSlot::query()->findOrFail($id)->update($params);

        return redirect()->route('admin.field.edit',['venue'=>$field->venue_id ,
            'field'=>$field->_id]);
    }


    public function getFieldProperties($sport_id , $field_id = null ){
        $sport = Sport::query()->find($sport_id);
        if ( $field_id != null )
        $field = Field::query()->find($field_id);
        else
            $field = null ;
        return view('admin.venue.property.field.field-inner', compact('sport','field'));
    }
}
