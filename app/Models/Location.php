<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes, WidgetRender;



    public $route = "location";

    public $title = "Locations";

    protected $fillable = [
        'name', 'country_id','status_id
        '
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'name',
            'type' => 'field',
            'db_name' => 'name'
        ],
        [
            'key' => 'country',
            'show' => 'name',
            'title' => 'Country',
            'type' => 'object',
            'chains' => 'country',
            'db_name' => 'name'
        ],

    ];

    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => 'true',
            'classes' => '',
            'insertion_type'=>'field'
        ],
        'country_id' => [
            'references' => 'App\\Models\\Country',
            'input' => 'select',
            'label' => 'Country',
            'id' => 'country_id',
            'name' => 'country_id',
            'isRequired' => false,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'country',
            'rules'=>'required',
            'insertion_type'=>'belongsTo'
        ],
        'status_id' => [
            'references' => 'App\\Models\\Status',
            'input' => 'select',
            'label' => 'Status',
            'id' => 'status_id',
            'name' => 'status_id',
            'isRequired' => false,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'status',
            'insertion_type'=>'belongsTo'
        ],
    ];

    public function country()
    {
        return $this->belongsTo(Country::class,'country_id');
    }


    public function status()
    {
        return $this->belongsTo(Status::class,'status_id');
    }


}
