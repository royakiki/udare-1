<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Registration extends Model
{
    //
    use SoftDeletes, WidgetRender;


    protected $fillable =['text'];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];


}
