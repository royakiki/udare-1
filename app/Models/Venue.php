<?php

namespace App\Models;

use App\User;
use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class Venue extends Model
{
    use SoftDeletes, WidgetRender;

    public $route = "venue";

    public $title = "Venues";


    protected $fillable = [
        'name', 'image', 'commercial_name','owner_id','location_id',
        'description','country_id','status_id','selection_ids'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'name',
            'type' => 'field',
            'selection' => 'name',
            'db_name' => 'name'
        ],
        [
            'key' => 'owner',
            'show' => 'name',
            'title' => 'Owner',
            'type' => 'object',
            'chains' => 'owner',
            'db_name' => 'name'
        ],
        [
            'key' => 'status',
            'show' => 'name',
            'title' => 'Status',
            'type' => 'object',
            'chains' => 'status',
            'db_name' => 'name'
        ],

    ];


    public $formFields = [

    ];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function owner(){
        return $this->belongsTo(User::class, 'owner_id');
    }


    public function sports()
    {
        return $this->belongsToMany(Sport::class)->with('category');
    }
    public function country(){
        return $this->belongsTo(Country::class, 'country_id');
    }
    public function location(){
        return $this->belongsTo(Location::class, 'location_id');
    }
    public function selections(){
        return $this->belongsToMany(Selection::class ,'selection_ids');
    }
    public function courts(){
        return  $this->hasMany(Field::class);
    }
    public function memberships(){
        return $this->hasMany(Membership::class);
    }

    public function trainers(){
        return $this->hasMany(Trainer::class);
    }
    public function moderators(){
        return $this->hasMany(User::class,'moderator_ids');
    }

}
