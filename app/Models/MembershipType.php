<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class MembershipType extends Model
{
    use SoftDeletes, WidgetRender;

    public $route = "membership_type";

    public $title = "MemberShip Types";


    protected $fillable = [
        'name', 'validity_days','expiry_days',
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'name',
            'type' => 'field',
            'selection' => 'name',
            'db_name' => 'name'
        ],
        [
            'key' => 'validity_days',
            'title' => 'Validity Days',
            'type' => 'field',
            'selection' => 'validity_days',
            'db_name' => 'validity_days'
        ],[
            'key' => 'expiry_days',
            'title' => 'Expiry Days',
            'type' => 'field',
            'selection' => 'expiry_days',
            'db_name' => 'expiry_days'
        ]

    ];

    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'validity_days' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Validity Days',
            'id' => 'validity_days',
            'name' => 'validity_days',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'expiry_days' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Expiry Days',
            'id' => 'expiry_days',
            'name' => 'expiry_days',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],

        'status_id' => [
            'references' => 'App\\Models\\Status',
            'input' => 'select',
            'label' => 'Status',
            'id' => 'status_id',
            'name' => 'status_id',
            'isRequired' => false,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'status',
            'insertion_type'=>'belongsTo'
        ],
    ];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

}
