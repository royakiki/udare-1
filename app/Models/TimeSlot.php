<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class TimeSlot extends Model
{
    //
    use SoftDeletes, WidgetRender;

    public $route='slot';
    public $title= 'Time Slot';

    protected $fillable =['field_id','start_time', 'end_time','price','status_id',
        'booking_limit','day','break_minutes','description','expertise_level','selection','max','min'];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'start_time',
            'title' => 'Start Time',
            'type' => 'field',
            'db_name' => 'start_time'
        ],
        [
            'key' => 'end_time',
            'title' => 'End Time',
            'type' => 'field',
            'db_name' => 'start_time'
        ],
    ];


    public $formFields = [
        'day' => [
            'references' => [''=>'Select Day'
                ,'monday' =>'Monday'
                ,'tuesday' =>'Tuesday'
                ,'wednesday' =>'Wednesday'
                ,'thursday' =>'Thursday'
                ,'friday' =>'Friday'
                ,'saturday' =>'Saturday'
                ,'sunday' =>'Sunday'],
            'input' => 'select',
            'label' => 'Slot Day',
            'id' => 'day',
            'name' => 'day',
            'isRequired' => 'true',
            'displayMember' => 'name',
            'valueMember' => 'id',
            'insertion_type'=>'field'
        ],
        'start_time' => [
            'input' => 'textbox',
            'type' => 'time',
            'label' => 'Start Time',
            'id' => 'start_time',
            'name' => 'start_time',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'end_time' => [
            'input' => 'textbox',
            'type' => 'time',
            'label' => 'End Time including break minutes',
            'id' => 'end_time',
            'name' => 'end_time',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'price' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Price',
            'id' => 'price',
            'name' => 'price',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'description' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Description',
            'id' => 'description',
            'name' => 'description',
            'isRequired' => false,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'booking_limit' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Maximum Booking Limit',
            'id' => 'booking_limit',
            'name' => 'booking_limit',
            'isRequired' => false,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'break_minutes' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Break in minutes',
            'id' => 'break_minutes',
            'name' => 'break_minutes',
            'isRequired' => false,
            'classes' => '',
            'insertion_type'=>'field'
        ],
        'expertise_level' => [
            'references' => [''=>'Select Level'
                ,'beginner' =>'Beginner'
                ,'intermediate' =>'Intermediate'
                ,'expert' =>'Expert'],
            'input' => 'select',
            'label' => 'Expertise Level',
            'id' => 'expertise_level',
            'name' => 'expertise_level',
            'isRequired' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'insertion_type'=>'field'
        ],
        'max' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Maximum Allowed Participants',
            'id' => 'max',
            'name' => 'max',
            'isRequired' => false,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'min' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Minimum Allowed Participants',
            'id' => 'min',
            'name' => 'min',
            'isRequired' => false,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'status_id' => [
            'references' => 'App\\Models\\Status',
            'input' => 'select',
            'label' => 'Status',
            'id' => 'status_id',
            'name' => 'status_id',
            'isRequired' => false,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'status',
            'insertion_type'=>'field'
        ],
    ];

    public function field(){
        return $this->belongsTo(Field::class,'field_id');
    }
    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }


}
