<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes, WidgetRender;

    public $route = "category";

    public $title = "Categories";


    protected $fillable = [
        'name', 'image', 'status'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'name',
            'type' => 'field',
            'selection' => 'name',
            'db_name' => 'name'
        ],
        [
            'key' => 'status',
            'title' => 'Status',
            'type' => 'field',
            'selection' => 'status',
            'db_name' => 'name'
        ],
    ];

    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => true,
            'classes' => '',
            'rules' => 'required',
            'insertion_type' => 'field'
        ],

        'image' => [
            'input' => 'file',
            'type' => 'file',
            'label' => 'Image Link',
            'id' => 'logo',
            'name' => 'image',
            'isRequired' => false,
            'custom' => true,
            'insertion_type' => 'field',
        ],

        'status_id' => [
            'references' => 'App\\Models\\Status',
            'input' => 'select',
            'label' => 'Status',
            'id' => 'status_id',
            'name' => 'status_id',
            'isRequired' => true,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'status',
            'insertion_type' => 'belongsTo'
        ],
    ];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
    public function sports()
    {
        return $this->hasMany(Sport::class, 'parent_id');
    }

}
