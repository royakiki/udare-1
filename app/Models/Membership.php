<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class Membership extends Model
{
    use SoftDeletes, WidgetRender;

    public $route='membership';
    public $title= 'MemberShips';

    protected $fillable =['sport_id','membership_type_id', 'discount_percentage','discount_days','status_id','venue_id',
        'description','name','image'];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'Name',
            'type' => 'field',
            'db_name' => 'name'
        ],
        [
            'key' => 'type',
            'show' => 'name',
            'title' => 'Type',
            'type' => 'object',
            'chains' => 'type',
            'db_name' => 'name'
        ],
        [
            'key' => 'status',
            'show' => 'name',
            'title' => 'Status',
            'type' => 'object',
            'chains' => 'status',
            'db_name' => 'name'
        ],
    ];


    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'membership_type_id' => [
            'references' => 'App\\Models\\MembershipType',
            'input' => 'select',
            'label' => 'Type',
            'id' => 'membership_type_id',
            'name' => 'membership_type_id',
            'isRequired' => true,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'type',
            'insertion_type'=>'field'
        ],
        'sport_id' => [
            'references' => 'App\\Models\\Sport',
            'input' => 'select',
            'label' => 'Type',
            'id' => 'sport_id',
            'name' => 'sport_id',
            'isRequired' => true,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'type',
            'insertion_type'=>'field'
        ],
        'discount_percentage' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Discount Percentage',
            'id' => 'discount_percentage',
            'name' => 'discount_percentage',
            'isRequired' => false,
            'classes' => '',
            'insertion_type'=>'field'
        ],
        'discount_days' => [
            'input' => 'textbox',
            'type' => 'number',
            'label' => 'Discount Days',
            'id' => 'discount_days',
            'name' => 'discount_days',
            'isRequired' => false,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'description' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Description',
            'id' => 'description',
            'name' => 'description',
            'isRequired' => false,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'image' => [
            'input' => 'file',
            'type' => 'file',
            'label' => 'Image Link',
            'id' => 'logo',
            'name' => 'image',
            'isRequired' => false,
            'custom' => true,
            'insertion_type'=>'field',
        ],
        'status_id' => [
            'references' => 'App\\Models\\Status',
            'input' => 'select',
            'label' => 'Status',
            'id' => 'status_id',
            'name' => 'status_id',
            'isRequired' => false,
            'withoutChooseOption' => true,
            'multiple' => false,
            'displayMember' => 'name',
            'valueMember' => 'id',
            'pivot_reference' => 'status',
            'insertion_type'=>'field'
        ],
    ];

    public function type(){
        return $this->belongsTo(MembershipType::class,'membership_type_id');
    }
    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }

    public function sport(){
        return $this->belongsTo( Sport::class , 'sport_id');
    }

    public function venue(){
        return $this->belongsTo( Venue::class , 'venue_id');
    }




}
