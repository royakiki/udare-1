<?php

namespace App\Models;

use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Team extends Model
{
    //
    use SoftDeletes, WidgetRender;

    public $route = "team";
    public $title = "Team Members";

    protected $fillable =['name','position','biography','image','order'];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'Name',
            'type' => 'field',
            'db_name' => 'name'
        ],
        [
            'key' => 'position',
            'title' => 'Position',
            'type' => 'field',
            'db_name' => 'position'
        ],
    ];
    public $formFields = [
        'name' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Name',
            'id' => 'name',
            'name' => 'name',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'position' => [
            'input' => 'textbox',
            'type' => 'text',
            'label' => 'Position',
            'id' => 'position',
            'name' => 'position',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'biography' => [
            'input' => 'textarea',
            'type' => 'text',
            'label' => 'Biography',
            'id' => 'biography',
            'name' => 'biography',
            'isRequired' => true,
            'classes' => '',
            'rules'=>'required',
            'insertion_type'=>'field'
        ],
        'image' => [
            'input' => 'file',
            'type' => 'file',
            'label' => 'Image Link',
            'id' => 'logo',
            'name' => 'image',
            'isRequired' => true,
            'custom' => true,
            'insertion_type'=>'field',
        ],
    ];
}
