<?php

namespace App\Models;

use App\User;
use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Builder;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Psy\Util\Str;


class VenueModerator extends User
{
    //
    use SoftDeletes, WidgetRender;

    protected $table = "users";


    public static function boot()
    {
        parent::boot();
        $role = Role::query()->where('slug', 'venue-moderator')->first()->_id;
        static::addGlobalScope('venueModeratorRole', function (Builder $builder) use ($role)  {
            $builder->where('role_id', $role );

        });
    }


    public function role(){
        return $this->belongsTo(Role::class);
    }


}
