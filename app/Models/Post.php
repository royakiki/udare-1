<?php

namespace App\Models;

use App\User;
use App\Utils\WidgetRender;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class Post extends Model
{
    use SoftDeletes, WidgetRender;

    public $route = "post";

    public $title = "Posts";


    protected $fillable = [
        'name', 'image', 'youtube_link','owner_id','venue_id',
        'description','country_id','status_id'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public $fields = [
        [
            'key' => 'name',
            'title' => 'name',
            'type' => 'field',
            'selection' => 'name',
            'db_name' => 'name'
        ],
        [
            'key' => 'owner',
            'show' => 'name',
            'title' => 'Owner',
            'type' => 'object',
            'chains' => 'owner',
            'db_name' => 'name'
        ],
        [
            'key' => 'status',
            'show' => 'name',
            'title' => 'Status',
            'type' => 'object',
            'chains' => 'status',
            'db_name' => 'name'
        ],

    ];


    public $formFields = [

    ];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function owner(){
        return $this->belongsTo(User::class, 'owner_id');
    }
    public function venue(){
        return $this->belongsTo(Venue::class, 'venue_id');
    }


    public function country(){
        return $this->belongsTo(Country::class, 'country_id');
    }
    public function location(){
        return $this->belongsTo(Location::class, 'location_id');
    }


}
