<?php

namespace App\Models;
use App\User;
use App\Utils\WidgetRender;

use Illuminate\Support\Facades\Log;
use Jenssegers\Mongodb\Eloquent\Builder;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class Trainer extends User
{
    //
    use SoftDeletes, WidgetRender;

    protected $table = "users";


    public static function boot()
    {
        parent::boot();
        $role = Role::query()->where('slug', 'trainer')->first()->_id;
        static::addGlobalScope('trainerRole', function (Builder $builder) use ($role)  {
            $builder->where('role_id', $role );

        });
    }


    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function sports(){
        return $this->belongsToMany(Sport::class);
    }

    public function venues(){
        return $this->belongsToMany(Venue::class);
    }
}
