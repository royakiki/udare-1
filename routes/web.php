<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('venue-request', 'Front\HomeController@registerForm')->name('request-venue');
Route::post('venue-request', 'Front\HomeController@register')->name('request-venue');
Route::get('venue-categories/{venue_id}', 'Front\HomeController@categoriesForm')->name('request-venue-categories');
Route::post('venue-categories', 'Front\HomeController@categories')->name('request-venue-categories.post');

Route::get('country-phone-code', 'CMS\CountryController@getPhoneCode')->name('country.phone.code');

Route::get('/select-ajax/{model?}', 'CMS\CmsController@ajaxSelectByModel')->name('select.ajax');
Auth::routes();
