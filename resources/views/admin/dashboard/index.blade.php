@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="col-md-12 padding-0">
    <div class="row">
        <div class="col-lg-6 post-main-box">
            <div class="row add-post-box" style="cursor: pointer"  onclick="newPostToggle()">
                <div class="col-md-12 add-post">
                    create new post...
                </div>
            </div>
            <div class="row">
                <div class="post-box" style="display: none">
                    <div class="col-md-12" style="padding: 0">
                        <img id="post-display" src="http://placehold.it/180" style="display: none"  />
                    </div>
                    <div class="col-md-12" style="padding: 0">
                        <textarea class="col-md-12 post-text"></textarea>
                    </div>
                    <div class="col-md-6" style="padding: 0">
                        <input id='post-image' type='file' onchange="readURL(this);" style="display: none"/>
                        <label for="post-image" class="post-attachment"><i class="fa fa-picture-o"></i> </label>
                    </div>
                    <div class="col-md-3" style="padding: 0" >
                        <div class="post-btn" onclick="newPostToggle()">cancel</div>
                    </div>
                    <div class="col-md-3" style="padding: 0">
                        <div class="post-btn">Post</div>
                    </div>



                </div>
            </div>

        </div>
    </div>
        </div>
    </div>



@endsection
@push('css')
<style>
    .add-post {
        background: white;
        width: 80%;
        border: 1px solid #dfdfdf;
        border-radius: 3px;
        margin: 10px 0;
        padding: 5px;
        font-size: 14px;
        font-family: 'ubuntu';
    }

    .post-box {
        width: 80%;
    }

    .post-text {
        background: white;
        width: 100%;
        border: 1px solid #dfdfdf;
        border-radius: 3px;
        min-height: 150px;
        margin: 10px 0;
        padding: 5px;
        font-size: 14px;
        font-family: 'ubuntu';
    }
    .post-btn {
        float: right;
        padding: 5px 20px;
        border-radius: 11px;
        background-color: #cb5d8a;
        color: white;
    }

</style>
@endpush
@push('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#post-display').show()
                $('#post-display')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function newPostToggle() {
        $('.add-post-box').toggle()
        $('.post-box').toggle()
    }
</script>
@endpush
