@extends('admin.layouts.app')

@section('content')
    <div class="">
        <div class="page-title">
            {{--            <div class="title_left">--}}
            {{--                <h3>User Settings</h3>--}}
            {{--            </div>--}}

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User Settings</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <form method="POST" action="{{ route('user.settings.store') }}"
                              data-parsley-validate="" class="form-horizontal form-label-left"
                              novalidate enctype="multipart/form-data">
                            @csrf


                            {{--                            <div class="form-group">--}}
                            {{--                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">dfsd--}}

                            {{--                                </label>--}}
                            {{--                                <div class="col-md-6 col-sm-6 col-xs-12">--}}
                            {{--                                    <input type="text" id="s" name="s"--}}
                            {{--                                           value="{{ isset($value) ? $value : old('s') }}"--}}

                            {{--                                           class="form-control col-md-7 col-xs-12 @isset($classes) {{ $classes }} @endisset ">--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                    Choose currency
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="currency_id" id="currency_id">
                                        <option value="">Choose currency</option>
                                        @foreach($options as $option)
                                            <option value="{{ $option->id }}"
                                                    @if (session()->get('preferred_currency') == $option->id) selected @endif>{{ $option->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-primary" type="reset">Reset</button>
                                    <button type="submit" class="btn btn-success btn-submit-model">Submit</button>
                                </div>
                            </div>


                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')

@endpush
