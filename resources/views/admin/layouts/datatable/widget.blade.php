@push('css')
    <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/datatables.net-select/css/select.dataTables.min.css')}}" rel="stylesheet">

    <link href="{{asset('vendors/sweet-alerts/sweetalert.css')}}" rel="stylesheet">
    {{--    <link href="https://nightly.datatables.net/buttons/css/buttons.dataTables.css?_=c6b24f8a56e04fcee6105a02f4027462.css" rel="stylesheet" type="text/css" />--}}
    <style>
        #table_processing {
            z-index: 100;
            height: 50px;
        }

        .dataTables_filter {
            width: unset;
        }

        .page-title .title_right .pull-right {
            margin: unset;
        }

        .buttons-colvis {
            height: 30px;
        }


    </style>

@endpush
{{--<input type="text" id="test">--}}

<table class=" table ajax-table table-bordered-bottom dataTable no-footer" id="table" cellspacing="0"
       width="100%" style="margin-top:0!important;">

    @include('admin.layouts.datatable.header')

</table>



@push('js')
    @include('admin.layouts.datatable.javascript')
@endpush
