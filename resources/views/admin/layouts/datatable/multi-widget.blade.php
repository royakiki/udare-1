@php($udid = uniqid())
<table class=" table ajax-table table-striped table-bordered dataTable no-footer" id="table{{$udid}}" cellspacing="0"
       width="100%" style="margin-top:0!important;">

    @include('admin.layouts.datatable.header')

</table>



@push('js')
@include('admin.layouts.datatable.multi-javascript',compact('udid'))
@endpush
