@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="col-md-12 padding-2">

            <div class="page-title">
                <div class="title_left">
                    <h5 style="color: #666B93;"><b>{{ $title ?? '' }}</b></h5>
                </div>
                @isset($date)
                    @push('css')
                        <style>
                            #cal-icon {
                                border-top-color: #ececec;
                                border-left-color: #ececec;
                                border-bottom-color: #ececec;
                            }

                            .daterangepicker {
                                left: 60% !important;
                                left: 63% !important;
                            }
                        </style>
                    @endpush
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                            <span class="add-on input-group-addon" id="cal-icon"><i
                                    class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" class="form-control" name="reservation-time" id="reservation-time">
                                <span class="input-group-btn">
                                <button class="btn btn-default" id="btn-date-search" type="button">Go</button>
                            </span>
                            </div>
                        </div>
                    </div>
                @endisset
                {{--                <div class="control-group text-left">--}}
                {{--                    <div class="controls">--}}
                {{--                        <div class="input-prepend input-group">--}}
                {{--                            <span class="add-on input-group-addon"><i--}}
                {{--                                    class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>--}}
                {{--                            <input style="width: 37%;" type="text" name="reservation-time" id="reservation-time"--}}
                {{--                                   class="form-control"/>--}}
                {{--                            <span class="input-group-btn">--}}
                {{--                      <button class="btn btn-default" type="button">Go!</button>--}}
                {{--                    </span>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group pull-right">
                            @isset($new)
                                <button type="button" style="" class="btn btn-round btn-success btn-new"
                                        onclick="window.location.href = '{{ $new }}'">New <i
                                        class="fa fa-plus"></i></button>
                            @endisset
                            <span class="input-group-btn">
{{--                      <button class="btn btn-default" type="button">Go!</button>--}}
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="font-size:20px;">
                    <div class="x_panel">
                        {{--                        <div class="x_title">--}}
                        {{--                            <h2>{{ $title }}--}}
                        {{--                            </h2>--}}
                        {{--                            <ul class="nav navbar-right panel_toolbox">--}}
                        {{--                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                        {{--                                </li>--}}
                        {{--                                <li class="dropdown">--}}
                        {{--                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
                        {{--                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                        {{--                                    <ul class="dropdown-menu" role="menu">--}}
                        {{--                                        <li><a href="#">Settings 1</a>--}}
                        {{--                                        </li>--}}
                        {{--                                        <li><a href="#">Settings 2</a>--}}
                        {{--                                        </li>--}}
                        {{--                                    </ul>--}}
                        {{--                                </li>--}}
                        {{--                                <li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                        {{--                                </li>--}}
                        {{--                            </ul>--}}
                        {{--                            <div class="clearfix"></div>--}}
                        {{--                        </div>--}}
                        <div class="x_content">

                            @include('admin.layouts.datatable.widget')

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('css')
    <style>
        {{--        .dataTables_filter {--}}
        {{--            width: unset;--}}
        {{--        }--}}

        {{--        .page-title .title_right .pull-right {--}}
        {{--            margin: unset;--}}
        {{--        }--}}
    </style>
@endpush
