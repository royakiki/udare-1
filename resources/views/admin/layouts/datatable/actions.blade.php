@if(empty($actions) || in_array('show',$actions))
    <a href="@if($baseRoute != null){{  $baseRoute.'/'.$ob->id }}@else {{ route($route.'.show',$ob->id) }} @endif" class="btn btn-success waves-effect">
        <i class="fa fa-eye"></i>
    </a>
@endif
@if(empty($actions) || in_array('edit',$actions))
    <a href="@if($baseRoute != null ){{  $baseRoute.'/'.$ob->id.'/edit' }}@else {{ route($route.'.edit',$ob->id) }} @endif" class="btn btn-info waves-effect">
        <i class="fa fa-edit"></i>
    </a>
@endif
{{--custom buttons--}}
{{--@isset($actions['custom'])--}}
{{--    @foreach($actions['custom'] as $action)--}}
{{--        @php($explodedElem = explode(',',$action))--}}
{{--        <a href="{{ route($route.'.'.$explodedElem[0],$ob->id) }}" class="btn btn-info waves-effect"--}}
{{--           style="display: inline;"><i class="{{$explodedElem[1]}}"></i></a>--}}
{{--    @endforeach--}}
{{--@endisset--}}

@isset($actions['custom'])
    @foreach($actions['custom'] as $action)
        <a title="{{ $action['title'] }}" href="{{ sprintf($action['route'], $ob->id) }}"
           class="btn btn-info waves-effect"
           style="display: inline;"><i class="{{ $action['icon'] }}"></i></a>
    @endforeach
@endisset
{{--end custom buttons--}}

@if(empty($actions) || in_array('delete',$actions))
    <form action="@if($baseRoute != null ){{  $baseRoute.'/'.$ob->id}}@else {{ route($route.'.destroy',$ob->id) }} @endif" method="POST" style="display: inline;">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger waves-effect btn-delete-model" type="button">
            <i class="fa fa-trash"></i>
        </button>
    </form>
@endif


