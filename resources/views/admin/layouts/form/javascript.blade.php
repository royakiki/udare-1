<script>
    $(function () {

        var k = "";

        function rgb2hex(rgb) {
            rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
            return (rgb && rgb.length === 4) ? "#" +
                ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
        }

        $('.btn-submit-model').on('click', function (e) {
            if (typeof $('.colorpicker').val() !== "undefined") {
                if ($('.colorpicker').val().indexOf('#') == -1) {
                    var hex = rgb2hex($('.colorpicker').val());
                    $('.colorpicker').val(hex);
                }
            }
            if (typeof $('#hdn').val() !== "undefined")
                $('#hdn').val(k);

        });
    });

    function ajaxSelect(id,title, query = null) {
        let route = $('#' + id).attr('data-route');
        let name = $('#' + id).attr('data-name');
        let value = $('#' + id).attr('data-value');
        if ($('#' + id).attr('data-flag') !== undefined) {
            let flag = $('#' + id).attr('data-search');
            route = route + '?search=' + flag;
        }
        if (query != null) {
            route += route.includes("?") ? ("&" + query) : ("?" + query)
        }
        $('#' + id).select2({
            width: '100%',
            placeholder: title,
            allowClear: true,
            ajax: {
                url: route,
                dataType: 'json',
                processResults: function (data) {
                    results = [];

                    // results.push({id: "", text:name})

                    data = $.map(data, function (obj) {
                        return {id: obj[value], text: obj[name]};
                    })
                    $.each(data, function (key, value) {
                        results.push(value)
                    })
                    return {
                        results: results
                    };
                },
                error: function (data) {
                    console.log(data)
                },
                cache: true
            }
        });

    }
</script>
<script>
    Dropzone.autoDiscover = false;
    $('.file-uploader').each(function(key, value){
        let route = $(value).attr('data-route');
        let routeRemove = $(value).attr('data-route-remove');
        let routeGet = $(value).attr('data-route-get');
        let multiple = $(value).attr('data-multiple');
        var uploadedDocumentMap = {}
        var options = Dropzone.options.documentDropzone = {
            url: route,
            maxFilesize: 20, // MB
            maxFiles: multiple?100:1,
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                // $('form').append('<input type="hidden" name="'+divname+(multiple?'[]':'')+'" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {

                $.ajax({
                    type: 'POST',
                    url: routeRemove,
                    data: {
                        _token : "{{ csrf_token() }}",
                        file : file.name,
                    },
                    dataType:'json',

                }).done(function (result)
                {
                    if ( result )
                        file.previewElement.remove()
                }).fail(function (data) {

                });
            },
            init: function () {
                function createThumbnail(temp) {
                    myDropzone.createThumbnailFromUrl(temp,
                        myDropzone.options.thumbnailWidth,
                        myDropzone.options.thumbnailHeight,
                        myDropzone.options.thumbnailMethod, true, function (thumbnail) {
                            myDropzone.emit('thumbnail', temp, thumbnail);
                            myDropzone.emit("complete", temp);
                        });
                }
                $.ajax({
                    type: 'GET',
                    url: routeGet,
                    data: {
                        _token : "{{ csrf_token() }}",
                    },
                    dataType:'json',

                }).done(function (result)
                {
                    for(let i =0 ;i < result.length; i++){
                        var mockFile = {name : result[i].name ,
                            size: result[i].size, type: result[i].type,
                            dataURL:'{{asset('storage')}}/'+result[i].url};
                        // uploadedDocumentMap.push(mockFile);
                        // myDropzone.emit('addedfile', mockFile);
                        // myDropzone.createThumbnailFromUrl(mockFile, mockFile['url']);
                        // myDropzone.emit('complete', mockFile);
                        // myDropzone._updateMaxFilesReachedClass();

                        myDropzone.emit("addedfile", mockFile);
                        createThumbnail(mockFile);
                        myDropzone.emit('complete', mockFile);
                    }
                }).fail(function (data) {

                });
            }
        }

        var myDropzone = new Dropzone(value,options );
    });

</script>
