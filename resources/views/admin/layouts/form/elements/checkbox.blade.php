<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="{{ $name }}">{{ $label }}
        {!!  $isRequired ? '<span class="required" style="color:red">*</span>' :  '' !!}
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="{{ $type }}" id="{{ $id }}" name="{{ $name }}"
               required="{{ $isRequired ? $isRequired : 'false' }}"
               @if(isset($value)&&$value==1)  checked @endif
               @if($isShow != 0) readonly="true" disabled @endif
               class="form-control col-md-7 col-xs-12 @isset($classes) {{ $classes }} @endisset ">
        @error($name)
        <span style="color:red">
                    {{ $message }}
                </span>
        @enderror
    </div>
</div>


