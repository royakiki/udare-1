@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        @if(!isset($disable_list_link) || (isset($disable_list_link) && $disable_list_link))
                            <li>
                                <a href="@if(!isset($action)){{ route(str_replace(' ','_',strtolower(Str::singular($route)).'.index')) }}@else {{ $action }}@endif">
                                    <i class="fa fa-list"></i>
                                    View List Of @if(!isset($linkDisplay)){{ $title }} @else {{ $linkDisplay }} @endif
                                </a>
                            </li>
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                </div><!-- /.x_title -->

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form method="POST" action="{{route('venue.store')}}" class="form-horizontal form-label-left"
                              enctype="multipart/form-data">
                            @csrf
                            <div  type="button" class="clp" >
                                <h3>Overview</h3>
                                <hr class="rounded" style="background-color: #E6E9ED">
                            </div>

                            <div id="overview" class="row clpc">
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="name">
                                            Venue Name
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="name" name="name" required
                                                   value="{{old('name')}}" class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="commercial_name">
                                            Official Name
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="commercial_name" name="commercial_name" required
                                                   value="{{old('commercial_name')}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="email">
                                            Email Id
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="email" name="email" required value="{{old('email')}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="country_id">
                                            Country
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="country_id"
                                                    id="country_id"
                                                    data-route="{{route('select.ajax','Country')}}"
                                                    data-name="name"
                                                    data-value="_id"
                                                    onchange="ajaxSelect('location_id','Location', 'country_id='+ this.value)"
                                                    required>
                                                @if(old('country_id') != null)
                                                    <option value="{{old('country_id')}}" selected>
                                                        {{\App\Models\Country::find(old('country_id'))->name}}
                                                    </option>
                                                @endif
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="location_id">
                                            Location
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <select class="select2 select2-ajax"
                                                    name="location_ids[]"
                                                    id="location_id"
                                                    data-route="{{route('select.ajax','Location')}}"
                                                    data-name="name"
                                                    data-value="_id" required>
                                                @if(old('location_ids') != null)
                                                    @foreach(old('location_ids') as $location)
                                                        <option value="{{$location}}" selected>
                                                            {{\App\Models\Location::find($location)->name}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-4" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="phone">
                                            Contact Number
                                        </label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="phone" name="phone" required value="{{old('phone')}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                            </div><!-- /.row -->

                            <div type="button" class="clp">
                                <h3>Sports & Categories</h3>
                                <hr class="rounded" style="background-color: #E6E9ED">
                            </div>
                            <div id="sports" class="row clpc">
                                @foreach( $categories as $category)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="{{$category->_id}}">
                                                {{$category->name}}
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2 select2-ajax"
                                                        name="sport_ids[]"
                                                        id="{{$category->_id}}"
                                                        data-route="{{route('select.ajax','Sport')}}?parent_id={{$category->_id}}"
                                                        data-name="name"
                                                        data-value="_id"
                                                        multiple="multiple">
                                                    @if(old('sport_ids') != null)
                                                        @foreach(old('sport_ids') as $sport_id)
                                                            @if($category->_id == \App\Models\Sport::find($sport_id)->category->_id)
                                                                <option value="{{$sport_id}}" selected>
                                                                    {{\App\Models\Sport::find($sport_id)->name}}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div><!-- /.col-* -->
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                @endforeach
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="others">
                                            Suggestions
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="select2 select2-ajax-add"
                                                    name="other_sport_names[]"
                                                    id="other_sport"
                                                    multiple="multiple">
                                            </select>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-4" style="min-height: 60px">
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-12 col-xs-12" for="selection_ids">
                                                Select Sections
                                            </label>
                                            <div class="col-md-8 col-sm-12 col-xs-12">
                                                <select class="select2 select2-ajax"
                                                        name="selection_ids[]"
                                                        id="selection_ids"
                                                        data-route="{{route('select.ajax','Selection')}}"
                                                        data-name="name"
                                                        data-value="_id" multiple required>
                                                    @if(old('selection_ids') != null)
                                                        @foreach(old('selection_ids') as $selection)
                                                            <option value="{{$selection}}" selected>
                                                                {{\App\Models\Selection::find($selection)->name}}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div><!-- /.col-* -->
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->

                            </div><!-- /.row -->
                            <div class="form-group">
                                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-10">
                                    <button type="submit" class="btn btn-success btn-submit-model">
                                        Create
                                    </button>
                                </div><!-- /.col-* -->
                            </div><!-- /.form-group -->
                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.x_panel -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
@endsection
@push('js')
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    @include('cms.layouts.form.javascript')
    <script>

        //Location
        @if(old('country_id') != null)
        //Preserve value if an error is returned onsubmission
        ajaxSelect('location_id','Location', 'country_id={{old("country_id")}}');
        @else
        //Initialize Locations (empty)
        ajaxSelect('location_id','Location', 'country_id=');
        @endif

        //Country
        ajaxSelect('country_id', 'Country')
        $('#country_id').on('change', function () {
            //Clear selected Locations when Country is changed
            $('#location_id').val(null).trigger("change");
        });
        //Selection
        ajaxSelect('selection_ids','Selections')
        //Status
        // ajaxSelect('status_id','Status')
        //Owner
        // ajaxSelect('owner_id','Owner')
        //Category
        @foreach( $categories as $category)
        ajaxSelect('{{$category->_id}}','Category')
        @endforeach
        //Category (Other)
        $(".select2-ajax-add").select2({
            tags: true,
            width: '100%',
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            }
        });
    </script>
@endpush
