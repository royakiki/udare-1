@extends('admin.layouts.app')
@push('css')
    <style>
        [type="file"] {
            height: 0;
            overflow: hidden;
            width: 0;
        }

        [type="file"] + label {
            background: #f15d22;
            border: none;
            border-radius: 5px;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-family: 'Poppins', sans-serif;
            font-size: inherit;
            font-weight: 600;
            margin-bottom: 1rem;
            outline: none;
            padding: 1rem 50px;
            position: relative;
            transition: all 0.3s;
            vertical-align: middle;
        }

        [type="file"] + label:hover {
            background-color: #d3460d;
        }

        [type="file"] + label.btn-2 {
            background-color: #99c793;
            border-radius: 50px;
            overflow: hidden;
        }

        [type="file"] + label.btn-2::before {
            color: #fff;
            content: "\f0ee";
            font-family: "FontAwesome";
            font-size: 100%;
            height: 100%;
            right: 130%;
            line-height: 3.3;
            position: absolute;
            top: 0px;
            transition: all 0.3s;
        }

        [type="file"] + label.btn-2:hover {
            background-color: #497f42;
        }

        [type="file"] + label.btn-2:hover::before {
            right: 75%;
        }
    </style>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> Account Details </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        {{--                        @if(!isset($disable_list_link) || (isset($disable_list_link) && $disable_list_link))--}}
                        {{--                            <li>--}}
                        {{--                                <a href="{{$route}}">--}}
                        {{--                                    <i class="fa fa-list"></i>--}}
                        {{--                                    View List Of @if(!isset($linkDisplay)){{ $title }} @else {{ $linkDisplay }} @endif--}}
                        {{--                                </a>--}}
                        {{--                            </li>--}}
                        {{--                        @endif--}}
                    </ul>
                    <div class="clearfix"></div>
                </div><!-- /.x_title -->

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form method="POST" action="{{route('admin.venue.account-details.post',$venueModel->_id)}}"
                              class="form-horizontal form-label-left">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6" >
                                    <div class="form-group" style="padding: 10px" >
                                        <label for="address_address">Address</label>
                                        <input type="text" id="address-input" name="address_address" class="form-control map-input">
                                        <input type="hidden" name="address_latitude" id="address-latitude" value="" />
                                        <input type="hidden" name="address_longitude" id="address-longitude" value="" />
                                        <div id="address-map-container" style="width:100%;height:400px; ">
                                            <div style="width: 100%; height: 100%" id="address-map"></div>
                                        </div>
                                    </div>

                                </div><!-- /.col-* -->


                                <div class="col-lg-6" style="min-height: 60px">
                                    <div id="socials" style="padding: 10px">
                                        <div id="socials-box"></div>
                                        <br/>
                                        <br/>
                                        <div class="btn btn-primary" onclick="addLink()">Add Social Link</div>
                                    </div>
                                </div><!-- /.col-* -->
                            </div><!-- row -->

                            <div class="form-group">
                                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-10">
                                    <button type="submit" class="btn btn-success btn-submit-model">
                                        Submit
                                    </button>
                                </div><!-- /.col-* -->
                            </div><!-- /.form-group -->
                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.x_panel -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
@endsection
@push('js')
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuBA_blyNZMwU-PgkI-ze6eVD1pNCuu9I&libraries=places&callback=initialize" async defer></script>

    @include('cms.layouts.form.javascript')
    <script type='text/javascript'>
        function initialize() {

            $('form').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });
            const locationInputs = document.getElementsByClassName("map-input");

            const autocompletes = [];
            const geocoder = new google.maps.Geocoder;
            for (let i = 0; i < locationInputs.length; i++) {

                const input = locationInputs[i];
                const fieldKey = input.id.replace("-input", "");
                const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';

                const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || -33.8688;
                const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || 151.2195;

                const map = new google.maps.Map(document.getElementById(fieldKey + '-map'), {
                    center: {lat: latitude, lng: longitude},
                    zoom: 13
                });
                const marker = new google.maps.Marker({
                    map: map,
                    position: {lat: latitude, lng: longitude},
                });

                marker.setVisible(isEdit);

                const autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.key = fieldKey;
                autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});
            }

            for (let i = 0; i < autocompletes.length; i++) {
                const input = autocompletes[i].input;
                const autocomplete = autocompletes[i].autocomplete;
                const map = autocompletes[i].map;
                const marker = autocompletes[i].marker;

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    marker.setVisible(false);
                    const place = autocomplete.getPlace();

                    geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            const lat = results[0].geometry.location.lat();
                            const lng = results[0].geometry.location.lng();
                            setLocationCoordinates(autocomplete.key, lat, lng);
                        }
                    });

                    if (!place.geometry) {
                        window.alert("No details available for input: '" + place.name + "'");
                        input.value = "";
                        return;
                    }

                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                });
            }
        }

        function setLocationCoordinates(key, lat, lng) {
            const latitudeField = document.getElementById(key + "-" + "latitude");
            const longitudeField = document.getElementById(key + "-" + "longitude");
            latitudeField.value = lat;
            longitudeField.value = lng;
        }
    </script>

    <script type="text/javascript">

        function addLink(){
            let counter = parseInt (Math.random()*1000);
            let div ='<div class="row" style="margin-bottom:20px">' +
                '<div class="col-md-5"><select class="select2 select2-without-ajax"' +
                '   name="social_link_ids[\''+counter+'\']"' +
                '   data-value ="_id">' ;
            div+='<option value="">Select trainer</option>';
            @foreach($links  as $link)
                div+='<option value="{{$link->_id}}">{{$link->name}}</option>';
            @endforeach
                div+= '</select></div>' +
                '<div class="col-md-5"><input type="text" name="social_link_value[\''+counter+'\']" required' +
                '   placeholder="value" class="form-control col-md-5"/></div>' +
                '<div class="col-md-2"><div class="btn btn-danger" onClick="$(this).parent().parent().remove()">remove</div></div>' +
                '</div>';
            $('#socials-box').append(div);
            $('.select2-without-ajax').select2({ width: '100%'});
        }
    </script>
@endpush
