@extends('admin.layouts.app')
@push('css')
    <style>
        [type="file"] {
            height: 0;
            overflow: hidden;
            width: 0;
        }

        [type="file"] + label {
            background: #f15d22;
            border: none;
            border-radius: 5px;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-family: 'Poppins', sans-serif;
            font-size: inherit;
            font-weight: 600;
            margin-bottom: 1rem;
            outline: none;
            padding: 1rem 50px;
            position: relative;
            transition: all 0.3s;
            vertical-align: middle;
        }

        [type="file"] + label:hover {
            background-color: #d3460d;
        }

        [type="file"] + label.btn-2 {
            background-color: #99c793;
            border-radius: 50px;
            overflow: hidden;
        }

        [type="file"] + label.btn-2::before {
            color: #fff;
            content: "\f0ee";
            font-family: "FontAwesome";
            font-size: 100%;
            height: 100%;
            right: 130%;
            line-height: 3.3;
            position: absolute;
            top: 0px;
            transition: all 0.3s;
        }

        [type="file"] + label.btn-2:hover {
            background-color: #497f42;
        }

        [type="file"] + label.btn-2:hover::before {
            right: 75%;
        }
    </style>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> Account Details </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        {{--                        @if(!isset($disable_list_link) || (isset($disable_list_link) && $disable_list_link))--}}
                        {{--                            <li>--}}
                        {{--                                <a href="{{$route}}">--}}
                        {{--                                    <i class="fa fa-list"></i>--}}
                        {{--                                    View List Of @if(!isset($linkDisplay)){{ $title }} @else {{ $linkDisplay }} @endif--}}
                        {{--                                </a>--}}
                        {{--                            </li>--}}
                        {{--                        @endif--}}
                    </ul>
                    <div class="clearfix"></div>
                </div><!-- /.x_title -->

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form method="POST" action="{{route('admin.venue.account-details.post',$venueModel->_id)}}"
                              class="form-horizontal form-label-left">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12"
                                               for="name">Account Holder's Name</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="name" name="name" required value="{{$venueModel->account_details['name']}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="account_number">Account Number</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="account_number" name="account_number" required
                                                   value="{{$venueModel->account_details['number']}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="currency">Currency</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="currency" name="currency" required
                                                   value="{{$venueModel->country->currency->name ?? ''}}"
                                                   class="form-control col-md-12 col-xs-12 " disabled/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="iban_number">IBAN Number</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="iban_number" name="iban_number" required
                                                   value="{{$venueModel->account_details['iban_number']}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="swift_code">Swift Code</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="text" id="swift_code" name="swift_code" required
                                                   value="{{$venueModel->account_details['swift_code']}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="files_ids">Other Files</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div id="files">
                                                <div id="files-box">
                                                    <div class="row" style="margin-bottom:20px">
                                                        <div class="col-md-10"><input type="text" name="other_files_names" required  placeholder="name" class="form-control col-md-5"/></div>
                                                        <div class="col-md-2"><div class="btn btn-danger" onClick="$(this).parent().parent().remove()">remove</div></div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 13px">
                                                            <input type="file" id="other_files'+counter+'" name="other_files[]" required/>
                                                            <label for="owner_passport'+counter+'" class="btn-2">upload</label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <br/>
                                                <br/>
                                                <div class="btn btn-primary" onclick="addFile()">Add File</div>
                                            </div>
                                        </div>
                                    </div>

                                </div><!-- /.col-* -->


                                <div class="col-lg-6" style="min-height: 60px">
                                    <div class="form-group">
                                        <label class="control-label col-md-12 col-sm-12 col-xs-12" style="text-align: left" >
                                            Bank Account Confirmation Letter</label>
                                        @isset($venueModel->account_details['confirmation_letter'])
                                            <a href="{{$venueModel->account_details['confirmation_letter']}}">
                                                Download File
                                            </a>
                                        @endisset
                                        <div class="col-md-12 col-sm-6 col-xs-12" >
                                            <input type="file" id="confirmation_letter" name="confirmation_letter"  required/>
                                            <label for="confirmation_letter" class="btn-2">upload</label>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="control-label col-md-12 col-sm-12 col-xs-12" style="text-align: left" >
                                            Trading License <span class="required" style="color:red">*</span></label>
                                        @isset($venueModel->account_details['trading_license'])
                                            <a href="{{$venueModel->account_details['trading_license']}}">
                                                Download File
                                            </a>
                                        @endisset
                                        <div class="col-md-12 col-sm-6 col-xs-12" >

                                            <input type="file" id="trading_license" name="trading_license"  required/>
                                            <label for="trading_license" class="btn-2">upload</label>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-12 col-xs-12" for="trading_license_expiry">Trading License Expiry</label>
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <input type="date" id="trading_license_expiry" name="trading_license_expiry" required
                                                   value="{{$venueModel->account_details['trading_license_expiry']}}"
                                                   class="form-control col-md-12 col-xs-12 "/>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="control-label col-md-12 col-sm-12 col-xs-12" style="text-align: left" >
                                            Copy of Owner's Id
                                            <span class="required" style="color:red">*</span>
                                        </label>
                                        @isset($venueModel->account_details['owner_id'])
                                            <a href="{{$venueModel->account_details['owner_id']}}">
                                                Download File
                                            </a>
                                        @endisset
                                        <div class="col-md-12 col-sm-6 col-xs-12" >
                                            <input type="file" id="owner_id" name="owner_id"  required/>
                                            <label for="owner_id" class="btn-2">upload</label>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label class="control-label col-md-12 col-sm-12 col-xs-12" style="text-align: left" >
                                            Copy of Owner's Passport
                                            <span class="required" style="color:red">*</span>
                                        </label>
                                        @isset($venueModel->account_details['owner_passport'])
                                            <a href="{{$venueModel->account_details['owner_passport']}}">
                                                Download File
                                            </a>
                                        @endisset
                                        <div class="col-md-12 col-sm-6 col-xs-12" >
                                            <input type="file" id="owner_passport" name="owner_passport"  required/>
                                            <label for="owner_passport" class="btn-2">upload</label>
                                        </div><!-- /.col-* -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- row -->

                            <div class="form-group">
                                <div class="col-md-2 col-sm-3 col-xs-12 col-md-offset-10">
                                    <button type="submit" class="btn btn-success btn-submit-model">
                                        Submit
                                    </button>
                                </div><!-- /.col-* -->
                            </div><!-- /.form-group -->
                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.x_panel -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->
@endsection
@push('js')
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    @include('cms.layouts.form.javascript')
    <script type='text/javascript'>
        function preview_image(event,imageHolderId)
        {
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById(imageHolderId);
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
@endpush
