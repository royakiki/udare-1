@component('mail::message')
# Introduction

The body of your message.

<a href="{{route('api.verify',$user->email_verification_token)}}">
Verify Account
</a>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
