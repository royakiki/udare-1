<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UDare</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}"
          rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/venue-owner/custom.css') }}" rel="stylesheet">
    <style>
        .row:before, .row:after {
            display: none;
        }
        #first-image {
            background-image: url('{{asset('img/login-1.png')}}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: right;
        }
        #second-image {
            background-image: url('{{asset('img/login-2.png')}}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: right;
        }
    </style>
    @toastr_css
</head>
<body>



<div id="login-carousel" class="carousel slide position-absolute d-none d-lg-block w-100" data-ride="carousel">
    <ol class="carousel-indicators">
        <li class="carousel-indicator active" data-target="#login-carousel" data-slide-to="0"><hr class="vertical-line white"></li>
        <li class="carousel-indicator" data-target="#login-carousel" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active" id="first-image">
            <div class="row p-lg-5 p-md-5 pt-5">
                <div class="col-lg-7 pt-5 offset-5">
                    <div id="welcome-section">
                        <h3 class="text-center text-white">Venues</h3>
                        <h2 class="text-center text-white ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                        <p class="mt-5 mb-3 text-center text-white ubuntu-regular" style="font-size: 1.3rem">We provide solutions to all the essentials that you need in order to manage and
                            grow yor Sports and Fitness
                            Venue business
                        </p>
                        <p class="text-center text-white ubuntu-light" style="font-size: 1.2rem; line-height:2.6rem" >
                            Branded Admin. Website Panel &amp App<br>
                            Online Bookings &amp Payment<br>
                            Single &amp Multi Venue Management<br>
                            Memberships &amp Day Passes<br>
                            Promotional Videos<br>
                            Sports Feed<br>
                            Customer Review &amp Ratings<br>
                            Staff &amp Roles Management<br>
                            Financial &amp Reports
                        </p>
                    </div>
                </div>
            </div>
        </div><!-- /.carousel-item -->

        <div class="carousel-item" id="second-image">
            <div class="row p-lg-5 p-md-5 pt-5">
                <div class="col-lg-7 pt-5 offset-5">
                    <div class="carousel-benefits-section">
                        <h3 class="text-center text-white">Venues</h3>
                        <h2 class="text-center text-white ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                        <div class="row">
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="100" width="100" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="100" width="100" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="100" width="100" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="100" width="100" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="100" width="100" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="100" width="100" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->

                        </div><!-- /.row -->
                    </div><!-- /.carousel-benefits-section -->
                </div>
            </div>
        </div><!-- /.carousel-item -->

    </div><!-- /.carousel-inner -->
</div><!-- /#login-carousel .carousel -->


<div class="container-fluid" id="register">
    <div class="row p-lg-5 p-md-4">

        <div class="col-lg-5 p-lg-3  p-md-0">
            <div class="card">
                <a href="{{route('home')}}">
                    <img width="135" height="165" src="{{asset('img/uDareLogo_ad.png')}}" class="img-rounded" alt="uDare">
                </a>
                <div class="card-body">
                    <form method="POST" action="{{route('request-venue-categories.post')}}">
                        @csrf
                        <input type="hidden" value="{{$venue_id}}" name="venue_id">

                        <div class="row">
                            <div class="col-md-12">
                                <h3>Sports Categories</h3>
                                <hr class="rounded" style="background-color: #38c172">
                            </div>
                        </div>
                        @foreach( $categories as $category)
                            <div class="row mt-2 p-2">
                                <label class="control-label col-12" for="{{$category->_id}}">{{$category->name}}</label>
                                <div class="col-12">
                                    <select class="select2 select2-ajax"
                                            name="sport_ids[]"
                                            id="{{$category->_id}}"
                                            data-route ="{{route('select.ajax','Sport')}}?parent_id={{$category->_id}}"
                                            data-name ="name"
                                            data-value ="_id"
                                            multiple="multiple">
                                    </select>
                                </div>
                            </div>
                        @endforeach
                        <div class="row mt-2 p-2">
                        <label class="control-label col-12" for="others">Add Any Suggestions</label>
                        <div class="col-12">
                            <select class="select2 select2-ajax-add"
                                    name="other_sport_names[]"
                                    id="other_sport"
                                    multiple="multiple">
                            </select>
                        </div>
                        </div>


                        <div class="row mt-2 mb-4 ">
                            <div class="col-sm-6 offset-6">
                                <button type="submit" id="register-btn" class="btn">{{ __('Submit') }}</button>
                            </div>
                        </div>
                    </form>
                </div><!-- /.card-body -->
            </div><!-- /.card -->
        </div><!-- /.col-* -->

        <div class="col-lg-7 pt-5 d-block d-lg-none">
            <div class="benefits-section">
                <h3 class="text-center">Venues</h3>
                <h2 class="text-center ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                <div class="row">

                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->



                </div><!-- /.row -->
            </div><!-- /.benefits-section -->
        </div><!-- /.col-* -->

    </div><!-- /.row -->
</div><!-- /#register .container-fluid -->





<!-- jQuery -->
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('front/js/bootstrap.min.js')}}"></script>

<script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
<script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="{{asset('js/custom.min.js')}}"></script>
@include('cms.layouts.form.javascript')
<script>
    @foreach( $categories as $category)
    ajaxSelect('{{$category->_id}}','Category')
    @endforeach
    $(".select2-ajax-add").select2({
        tags: true,
        width:'100%',
        createTag: function (params) {
            return {
                id: params.term,
                text: params.term,
                newOption: true
            }
        }
    });
</script>
<script>
    window.onload = responsiveImages;
    window.addEventListener("resize", responsiveImages);
    function responsiveImages(){
        var matches = document.querySelectorAll(".set-height");
        matches.forEach(function(match) {
            match.height = match.width;
        });
    }
    window.onload = r;
    window.addEventListener("resize", r);
    function r(){
        $('.carousel-item').addClass('d-lg-block')
        $('.carousel-item').height('auto');
        var matches = document.querySelectorAll(".carousel-item");
        var maxHeight = 0;
        matches.forEach(function(match) {
            if(maxHeight < match.clientHeight){
                maxHeight = match.clientHeight;
            }
        });
        $('.carousel-item').removeClass('d-lg-block')
        var form = document.querySelector("#register")
        var formHeight = form.clientHeight;
        if(maxHeight < formHeight){
            maxHeight = formHeight;
        }
        $('.carousel-item').height(maxHeight);
    }
</script>
</body>
@toastr_js
@toastr_render
</html>



