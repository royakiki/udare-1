<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UDare</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{ asset('vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}"
          rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/venue-owner/custom.css') }}" rel="stylesheet">
    <style>
        .row:before, .row:after {
            display: none;
        }
        #first-image {
            background-image: url('{{asset('img/login-1.png')}}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: right;
        }
        #second-image {
            background-image: url('{{asset('img/login-2.png')}}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: right;
        }
    </style>
    @toastr_css
</head>
<body>



<div id="login-carousel" class="carousel slide position-absolute d-none d-lg-block w-100" data-ride="carousel">
    <ol class="carousel-indicators">
        <li class="carousel-indicator active" data-target="#login-carousel" data-slide-to="0"><hr class="vertical-line white"></li>
        <li class="carousel-indicator" data-target="#login-carousel" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active" id="first-image">
            <div class="row p-lg-5 p-md-5 pt-5">
                <div class="col-lg-7 pt-5 offset-5">
                    <div id="welcome-section">
                        <h3 class="text-center text-white">Venues</h3>
                        <h2 class="text-center text-white ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                        <p class="mt-5 mb-3 text-center text-white ubuntu-regular" style="font-size: 1.3rem">We provide solutions to all the essentials that you need in order to manage and
                            grow yor Sports and Fitness
                            Venue business
                        </p>
                        <p class="text-center text-white ubuntu-light" style="font-size: 1.2rem; line-height:2.6rem" >
                            Branded Admin. Website Panel &amp App<br>
                            Online Bookings &amp Payment<br>
                            Single &amp Multi Venue Management<br>
                            Memberships &amp Day Passes<br>
                            Promotional Videos<br>
                            Sports Feed<br>
                            Customer Review &amp Ratings<br>
                            Staff &amp Roles Management<br>
                            Financial &amp Reports
                        </p>
                    </div>
                </div>
            </div>
        </div><!-- /.carousel-item -->

        <div class="carousel-item" id="second-image">
            <div class="row p-lg-5 p-md-5 pt-5">
                <div class="col-lg-7 pt-5 offset-5">
                    <div class="carousel-benefits-section">
                        <h3 class="text-center text-white">Venues</h3>
                        <h2 class="text-center text-white ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                        <div class="row">
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->
                            <div class="col-lg-4 mt-5">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img height="80" width="80" src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle w-50" alt="">
                                    </div>
                                    <div class="col-md-12">
                                        <h6 class="text-center text-white ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                        <p class="text-center text-white ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                                    </div>
                                </div>
                            </div><!-- /.col-* -->

                        </div><!-- /.row -->
                    </div><!-- /.carousel-benefits-section -->
                </div>
            </div>
        </div><!-- /.carousel-item -->

    </div><!-- /.carousel-inner -->
</div><!-- /#login-carousel .carousel -->

<div class="container-fluid" id="register">
    <div class="row p-lg-5 p-md-4">

        <div class="col-lg-5 p-lg-3  p-md-0">
            <div class="card">
               <a href="{{route('home')}}">
                   <img width="135" height="165" src="{{asset('img/uDareLogo_ad.png')}}" class="img-rounded" alt="uDare">
               </a>
                <div class="card-body">
                    <form method="POST" action="{{ route('request-venue') }}">
                        @csrf
                        @method('POST')
                        <div class="row p-2">
                        <input type="text" id="name" name="name" required value="{{old('name')}}"
                               class="form-control @error('name') is-invalid @enderror" placeholder="Owner Name*"/>
                        </div>
                        <div class="row mt-2 p-2">
                            <input type="text" id="venue_name" name="venue_name" required value="{{old('venue_name')}}" placeholder="Venue Name*" class="form-control @error('venue_name') is-invalid @enderror"/>
                        </div>
                        <div class="row mt-2 p-2">
                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                   placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                        </div>
                        <div class="row mt-2 p-2">
                        <select class="select2 select2-ajax"
                                name="country_id"
                                id="country_id"
                                data-route ="{{route('select.ajax','Country')}}"
                                data-name ="name"
                                data-placeholder="Country"
                                data-value ="_id"
                                onchange="ajaxSelect('location_id','Location', 'country_id='+ this.value);changePhoneCode(this.value)"
                                required>
                            @if(old('country_id') != null)
                                    <option value="{{\App\Models\Country::find(old('country_id'))->name}}" selected>
                                        {{\App\Models\Country::find(old('country_id'))->name}}
                                    </option>

                            @endif
                        </select>
                        </div>
                        <div class="row mt-2 p-2">
                        <select class="select2 select2-ajax"
                                name="location_id"
                                id="location_id"
                                data-placeholder="Location"
                                data-route ="{{route('select.ajax','Location')}}"
                                data-name ="name"
                                data-value ="_id" required>
                            @if(old('location_id') != null)
                                    <option value=" {{\App\Models\Location::find(old('location_id'))->name}}" selected>
                                        {{\App\Models\Location::find(old('location_id'))->name}}
                                    </option>
                            @endif
                        </select>
                        </div>
                        <div class="row mt-2 p-2">
                            <select class="select2 select2-ajax"
                            name="selection_ids[]"
                            id="selection_ids"
                            data-placeholder="Sections"
                            data-route ="{{route('select.ajax','Selection')}}"
                            data-name ="name"
                            data-value ="_id" multiple required>
                                @if(old('selection_ids') != null)
                                    @foreach(old('selection_ids') as $selection)
                                        <option value="{{$selection}}" selected>
                                            {{\App\Models\Selection::find($selection)->name}}
                                        </option>
                                    @endforeach
                                @endif
                    </select>
                        </div>
                        <div class="row mt-2">
                            <div class="col-2 text-left p-0">
                                <p class="phone-code"></p>
                            </div>
                            <div class="col-10">
                                <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone" name="phone" value="{{ old('phone') }}" required>
                            </div>
                        </div>
                        <div class="row mt-2 p-2">
                            <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror"
                               placeholder="Password" name="password" required autocomplete="new-password">
                        </div>
                        <div class="row mt-2 p-2">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                   placeholder="Confirm Password" required autocomplete="new-password">
                        </div>

                        <div class="row">
                            <div class="col-sm-6 offset-6">
                                <button type="submit" id="register-btn" class="btn">{{ __('Register') }}</button>
                            </div>
                        </div>
                    </form>
                    <div class="login-text text-center">
                        <p>An existing venue?<a href="{{route('login')}}">Log in</a></p>
                    </div>
                </div><!-- /.card-body -->
            </div><!-- /.card -->
        </div><!-- /.col-* -->

        <div class="col-lg-7 pt-5 d-block d-lg-none">
            <div class="benefits-section">
                <h3 class="text-center">Venues</h3>
                <h2 class="text-center ubuntu-medium">We warmly welcome you <br>to join uDare family</h2>
                <div class="row">

                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->
                    <div class="col-lg-4 mt-5">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{asset('img/login-benefit-placeholder.png')}}" class="rounded-circle set-height w-25" alt="">
                            </div>
                            <div class="col-md-12">
                                <h6 class="text-center ubuntu-regular mt-2">Increase Outreach &amp Sales</h6>
                                <p class="text-center ubuntu-light" style="line-height: 1.8rem">Get access to our largest sports community to increase outreach and boost your sales.</p>
                            </div>
                        </div>
                    </div><!-- /.col-* -->



                </div><!-- /.row -->
            </div><!-- /.benefits-section -->
        </div><!-- /.col-* -->

    </div><!-- /.row -->
</div><!-- /#register .container-fluid -->










<!-- jQuery -->
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('front/js/bootstrap.min.js')}}"></script>

<script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
<script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="{{asset('js/custom.min.js')}}"></script>
@include('cms.layouts.form.javascript')
<script>
    @if(old('country_id') != null)
    changePhoneCode("{{old('country_id')}}");
    @endif
    function changePhoneCode(countryId){
        $.ajax({
            type: 'GET',
            url: "{{ route('country.phone.code') }}",
            data: {countryId: countryId, _token: '{{csrf_token()}}'},
            success: function (data) {
                $(".phone-code").html('')
                $(".phone-code").append(data.phone_code)
            }
        });
    }
    ajaxSelect('country_id','Country')
    ajaxSelect('location_id','Location','country_id='+0)
    ajaxSelect('selection_ids','Selections')

    $(function () {
        $('.select2-without-ajax').select2({width: '100%'});
    })
</script>
<script>
    window.onload = responsiveImages;
    window.addEventListener("resize", responsiveImages);
    function responsiveImages(){
        var matches = document.querySelectorAll(".set-height");
        matches.forEach(function(match) {
            match.height = match.width;
        });
    }
    window.onload = r;
    window.addEventListener("resize", r);
    function r(){
        $('.carousel-item').addClass('d-lg-block')
        $('.carousel-item').height('auto');
        var matches = document.querySelectorAll(".carousel-item");
        var maxHeight = 0;
        matches.forEach(function(match) {
            if(maxHeight < match.clientHeight){
                maxHeight = match.clientHeight;
            }
        });
        $('.carousel-item').removeClass('d-lg-block')
       var form = document.querySelector("#register")
        var formHeight = form.clientHeight;
        if(maxHeight < formHeight){
            maxHeight = formHeight;
        }
        var innerHeight = window.innerHeight;
        if(innerHeight > maxHeight){
            maxHeight = innerHeight;
        }
        $('.carousel-item').height(maxHeight);
    }
</script>
</body>
@toastr_js
@toastr_render
</html>
