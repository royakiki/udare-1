<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0; background-color: #EDEDED;">
            <a href="{{ route('dashboard') }}" class="site_title"
               style="height: 102.5%; color: #2A3F54 !important; font-weight: bold;">
                <i class="fa fa-ball"></i>
                <span>uDare</span></a>
        </div>

        <div class="clearfix"></div>

        {{--        <!-- menu profile quick info -->--}}
        {{--        <div class="profile clearfix">--}}
        {{--            <div class="profile_pic">--}}
        {{--                <img src="images/img.jpg" alt="..." class="img-circle profile_img">--}}
        {{--            </div>--}}
        {{--            <div class="profile_info">--}}
        {{--                <span>Welcome,</span>--}}
        {{--                <h2>John Doe</h2>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        <!-- /menu profile quick info -->--}}

        <br/>

    @include('cms.layouts.sidebar')

    <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            {{--            <a data-toggle="tooltip" data-placement="top" title="Settings">--}}
            {{--                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>--}}
            {{--            </a>--}}
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            {{--            <a data-toggle="tooltip" data-placement="top" title="Lock">--}}
            {{--                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>--}}
            {{--            </a>--}}
            <a>
                <form action="{{route('logout')}}" method="POST">
                    @csrf
                    <button type="submit" style="background:inherit;border: inherit;font-size: inherit"
                            data-toggle="tooltip"
                            data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </button>
                </form>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        {{--<img src="images/img.jpg" alt="">--}}
                        {{ auth()->user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        {{--                        <li><a href="javascript:;"> Profile</a></li>--}}
                        {{--                        <li>--}}
                        {{--                            <a href="javascript:;">--}}
                        {{--                                <span class="badge bg-red pull-right">50%</span>--}}
                        {{--                                <span>Settings</span>--}}
                        {{--                            </a>--}}
                        {{--                        </li>--}}
                        {{--                        <li><a href="javascript:;">Help</a></li>--}}

                        {{--                        <li>--}}
                        {{--                            <a href="{{ route('user.settings') }}" class="text-center">--}}
                        {{--                                --}}{{--                                <span class="badge bg-red pull-right">50%</span>--}}
                        {{--                                <span>Settings</span>--}}
                        {{--                            </a>--}}
                        {{--                        </li>--}}
                        <li style="text-align: center"><a href="">My Profile <i
                                    class="fa fa-user"></i> </a></li>
                        <li>
                            <form method="post" action="{{route('logout')}}" class="text-center">
                                @csrf
                                <button type="submit" style="background:none;outline:none;border:none;padding:16px;">
                                    <i class="fa fa-sign-out pull-right"></i> Log Out
                                </button>
                            </form>
                        </li>

                    </ul>
                </li>

                {{--                <li role="presentation" class="dropdown">--}}
                {{--                    <a href="javascript:" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">--}}
                {{--                        <i class="fa fa-envelope-o"></i>--}}
                {{--                        <span class="badge bg-green">6</span>--}}
                {{--                    </a>--}}
                {{--                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">--}}
                {{--                        <li>--}}
                {{--                            <a>--}}
                {{--                                <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>--}}
                {{--                                <span>--}}
                {{--                          <span>John Smith</span>--}}
                {{--                          <span class="time">3 mins ago</span>--}}
                {{--                        </span>--}}
                {{--                                <span class="message">--}}
                {{--                          Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                {{--                        </span>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <a>--}}
                {{--                                <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>--}}
                {{--                                <span>--}}
                {{--                          <span>John Smith</span>--}}
                {{--                          <span class="time">3 mins ago</span>--}}
                {{--                        </span>--}}
                {{--                                <span class="message">--}}
                {{--                          Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                {{--                        </span>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <a>--}}
                {{--                                <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>--}}
                {{--                                <span>--}}
                {{--                          <span>John Smith</span>--}}
                {{--                          <span class="time">3 mins ago</span>--}}
                {{--                        </span>--}}
                {{--                                <span class="message">--}}
                {{--                          Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                {{--                        </span>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <a>--}}
                {{--                                <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>--}}
                {{--                                <span>--}}
                {{--                          <span>John Smith</span>--}}
                {{--                          <span class="time">3 mins ago</span>--}}
                {{--                        </span>--}}
                {{--                                <span class="message">--}}
                {{--                          Film festivals used to be do-or-die moments for movie makers. They were where...--}}
                {{--                        </span>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <div class="text-center">--}}
                {{--                                <a>--}}
                {{--                                    <strong>See All Alerts</strong>--}}
                {{--                                    <i class="fa fa-angle-right"></i>--}}
                {{--                                </a>--}}
                {{--                            </div>--}}
                {{--                        </li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
