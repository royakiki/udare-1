<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ $label }}
        {!!  isset($isRequired) && $isRequired==true ? '<span class="required" style="color:red">*</span>' :  '' !!}
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea class="form-control {{ isset($classes) ? $classes : ''}}" id="{{ $id }}" name="{{ $name }}" rows="3"
                  @if($isShow != 0) readonly="true" disabled @endif
                  required="{{ isset($isRequired) ? $isRequired : 'false' }}">{{ isset($value) ? $value : old($name) }}</textarea>
        @error($name)
        <span style="color:red">
        {{ $message }}
        </span>
        @enderror
    </div>
</div>
