<div class="form-group">
    <div style="width:100%;text-align: center;">
        <img
            src="@if(isset($custom) && isset($value)) {{ asset($value) }}
            @elseif(isset($custom) && !isset($value)) {{ asset('img/logo-placeholder.png') }}
            @elseif(!isset($company->logo) || is_null($company->logo) || empty($company->logo)){{ asset('img/logo-placeholder.png') }}
            @else {{ $company->logo }} @endif"
            name="image-holder" id="image-holder{{$id}}" height="150"
            width="150"
            style="display: block;margin: 0 26%;"><br>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12" style="padding-left: 17%;">

        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ $label }}
            {!!  $isRequired ? '<span class="required" style="color:red">*</span>' :  '' !!}
        </label>
        <input type="file" id="{{ $id }}" onchange="preview_image(event,'{{$id}}')" name="{{ $name  }}" @if($isShow != 0) readonly="true" disabled @endif />
        <label for="{{ $id }}" class="btn-2">upload</label>
        @error($name)
        <span style="color:red">
        {{ $message }}
        </span>
        @enderror
    </div>
</div>
