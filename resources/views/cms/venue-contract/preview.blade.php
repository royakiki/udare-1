<html>



<p>Dear {{$venueContract->venue->owner->name}}</p>
<br/>
<br/>
<br/>

{!! $venueContract->contract->description !!}

<br/>
<br/>
<br/>

<table>
    <tr>
    <td>Sport</td>
    <td>Commission</td>
    <td>Notes</td>
    </tr>
    @foreach($venueContract->sports as $sport)
        <tr>
            <td>{{$sport['name'] ?? ''}}</td>
            <td>{{$sport['commission'] ?? ''}}</td>
            <td>{{$sport['notes'] ?? ''}}</td>
        </tr>
    @endforeach
</table>


<p>Name:_____________________</p><br/>
<p>Date: ____________________</p>
</html>
