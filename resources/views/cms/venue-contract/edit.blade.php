@extends('cms.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        {{--                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                        @if(!isset($disable_list_link) || (isset($disable_list_link) && $disable_list_link))
                            <li>
                                <a href="@if(!isset($action)){{ route(str_replace(' ','_',strtolower(Str::singular($route)).'.index')) }}@else {{ $action }}@endif">
                                    <i class="fa fa-list"></i>
                                    View List Of @if(!isset($linkDisplay)){{ $title }} @else {{ $linkDisplay }} @endif
                                </a>
                            </li>
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>

                    <form method="POST" action="{{route('venue-contract.update', $venueContract->_id)}}" id="main-form"
                          data-parsley-validate="" class="form-horizontal form-label-left"
                          novalidate enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contract_id">Contract</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2 select2-ajax"
                                        name="contract_id"
                                        id="contract_id"
                                        data-route ="{{route('select.ajax','Contract')}}"
                                        data-name ="name"
                                        data-value ="_id"
                                >
                                    <option value="{{$venueContract->contract->_id??''}}" selected>
                                        {{$venueContract->contract->name??''}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="venue_id">Venue</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2 select2-ajax"
                                        name="venue_id"
                                        id="venue_id"
                                        data-route ="{{route('select.ajax','Venue')}}"
                                        data-name ="name"
                                        data-value ="_id"
                                >
                                    <option value="{{$venueContract->venue->_id??''}}" selected>
                                        {{$venueContract->venue->name??''}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status_id">Status</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2 select2-ajax"
                                        name="status_id"
                                        id="status_id"
                                        data-route ="{{route('select.ajax','Status')}}"
                                        data-name ="name"
                                        data-value ="_id"
                                        disabled
                                >
                                    <option value="{{$venueContract->status->_id??''}}" selected>
                                        {{$venueContract->status->name??''}}</option>
                                </select>
                            </div>
                        </div>
                        <br/>
                        <div class="ln_solid"></div>
                        <br/>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sport_ids">Sports and Commissions</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div id="sports">
                                    <div id="sports-box"></div>
                                    <br/>
                                    <br/>
                                    <div class="btn btn-primary" onclick="addSport()">Add Sport and Commission</div>
                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        @if ( $venueContract->status->slug != 'pending')
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Upload Signed Contract
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="needsclick dropzone file-uploader" id="dz-documents" data-route="{{route('cms.storeMedia')}}"
                                     data-name="contract" data-files="@if(is_array($venueContract->uploaded_contract)){{implode(',',$venueContract->uploaded_contract)}}@else{{$venueContract->uploaded_contract}}@endif">
                                    <div class="fallback">
                                        <input name="contract" type="file"  />
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button id="reset" class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit"
                                        class="btn btn-success btn-submit-model">Submit</button>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <a class="btn btn-primary"  target="_blank" href="{{route('venue-contract.preview',$venueContract->_id)}}" >Preview</a>
                                <a class="btn btn-danger" target="_blank" href="{{route('venue-contract.download',$venueContract->_id)}}"  >Download</a>
                                <div class="btn btn-success " style="cursor: pointer" onclick="sendEmail()" >Save and send to client</div>
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="{{asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/loudev-multiselect/css/multi-select.css')}}" rel="stylesheet">
    {{--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">--}}
    <style>
        [type="file"] {
            height: 0;
            overflow: hidden;
            width: 0;
        }

        [type="file"] + label {
            background: #f15d22;
            border: none;
            border-radius: 5px;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-family: 'Poppins', sans-serif;
            font-size: inherit;
            font-weight: 600;
            margin-bottom: 1rem;
            outline: none;
            padding: 1rem 50px;
            position: relative;
            transition: all 0.3s;
            vertical-align: middle;
        }

        [type="file"] + label:hover {
            background-color: #d3460d;
        }

        [type="file"] + label.btn-2 {
            background-color: #99c793;
            border-radius: 50px;
            overflow: hidden;
        }

        [type="file"] + label.btn-2::before {
            color: #fff;
            content: "\f0ee";
            font-family: "FontAwesome";
            font-size: 100%;
            height: 100%;
            right: 130%;
            line-height: 3.3;
            position: absolute;
            top: 0px;
            transition: all 0.3s;
        }

        [type="file"] + label.btn-2:hover {
            background-color: #497f42;
        }

        [type="file"] + label.btn-2:hover::before {
            right: 75%;
        }
    </style>
@endpush
@push('js')
    <script src="{{ asset('vendors/loudev-multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('js/ckeditor.js') }}"></script>
    @include('cms.layouts.form.javascript')
    <script>

        function sendEmail(){
            $("<input />").attr("type", "hidden")
                .attr("name", "email")
                .attr("value", "true")
                .appendTo("#main-form");
            $("#main-form").submit();
        }
        ajaxSelect('venue_id', 'Venue')
        ajaxSelect('contract_id', 'Contract')
        ajaxSelect('status_id','Status')

        function addSport(sport_id = null,name = null, commission = null , notes = null ){
            let counter = parseInt (Math.random()*1000);
            let div ='<div class="row" style="margin-bottom:20px">' +
                '<div class="col-md-5"><select class="select2 select2-ajax"' +
                '   name="sport_ids[\''+counter+'\']"' +
                '   id="sport_id_'+counter+'"' +
                '   data-route ="{{route('select.ajax','Sport')}}"' +
                '   data-name ="name"' +
                '   data-value ="_id">' +
                '</select></div>' +
                '<div class="col-md-5"><input type="text" name="sport_commissions[\''+counter+'\']"' ;
                if (commission!= null )
                    div +=' value="'+commission+'" ';

                div+= ' placeholder="%" class="form-control col-md-5"/></div>' +
                '<div class="col-md-2"><div class="btn btn-danger" onClick="$(this).parent().parent().remove()">remove</div></div>' +
                '<div class="col-md-12"><br/><input type="text" placeholder="additional note" class="form-control"' ;
                if (notes!= null )
                    div+=' value="'+notes+'" ';
                div+=' name="commission_notes[\''+counter+'\']" class=""/> </div></div>';
            $('#sports-box').append(div);
            ajaxSelect('sport_id_'+counter,'Sport')
            if ( sport_id != null )
                setTimeout( function () {
                    var newOption = new Option(name, sport_id, false, false);
                    $('#sport_id_'+counter).append(newOption).trigger('change');
                },500)
        }
        @foreach($venueContract->sports as $sport)

            addSport('{{$sport['sport_id']}}','{{$sport['name']}}','{{$sport['commission']}}','{{$sport['notes']}}')
        @endforeach
    </script>
@endpush
